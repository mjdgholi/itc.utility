﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace ITC.Library.Controls
{
  public   class CustomeRadMaskedTextBox : RadMaskedTextBox
    {    
      public string ClientScript = "true";
      public bool Focus { get; set; }

      RegularExpressionValidator regularExpressionValidator = new RegularExpressionValidator();
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            this.ToolTip = "تقویم";
            base.Render(writer);

      
       
            //regularExpressionValidator.RenderControl(writer);
            
        }


        protected override void OnInit(EventArgs e)
        {
            this.ButtonsPosition =InputButtonsPosition.Left;
            this.DisplayMask = "####/##/##";
            this.DisplayPromptChar = "_";            
            this.Height = 20;
            this.Width = 72;
            this.LabelCssClass = "radLabelCss_Default";
            this.Mask = "####/##/##";
            this.MaxLength = 10;
            this.NumericRangeAlign =NumericRangeAlign.Left;
            this.PromptChar = "_";
            if (Focus)
            this.Focus();
            this.SelectionOnFocus = SelectionOnFocus.SelectAll;
            this.Skin = "Windows7";
            this.DisplayFormatPosition = Telerik.Web.UI.DisplayFormatPosition.Left;                             
            this.ToolTip = "تاريخ به صورت 8 رقمي وارد شود : مثال 1387/01/01";

           //regularExpressionValidator = new RegularExpressionValidator();
           // regularExpressionValidator.ControlToValidate = this.ClientID;
           // regularExpressionValidator.ValidationExpression = @"\d{4}\/\d{2}/\d{2}";
           // regularExpressionValidator.ErrorMessage = "فرمت تاریخyyyy/mm/dd";
           // regularExpressionValidator.Text = "فرمت تاریخyyyy/mm/dd";
           // //regularExpressionValidator.ValidationGroup = this.ValidationGroup;
           // this.Controls.Add(regularExpressionValidator);

        }



      public  string AddDateSlash8Digit(string MyDate)
        {
            string str = "-";
            if (MyDate.Trim().Length != 0)
            {
                str = "";
                for (int i = 0; i < 8; i++)
                {
                    str += MyDate[i];
                    if (i == 3 || i == 5)
                        str += "/";
                }
            }
            if (str == "-") str = "";
            return str;
        }
    }
}
