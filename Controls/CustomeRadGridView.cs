﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.CustomeRadGridView.CustomeRadGridView.js", "text/javascript", PerformSubstitution = true)]
namespace ITC.Library.Controls
{

    public class CustomeRadGridView : RadGrid
    { 
        private bool scriptLoaded = false;
        public string CustomeClientDataTitle  { get; set; }
        public string CustomeClientDataKeyId { get; set; }
        public string CustomeTitle { get; set; } 
        

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {  
           if (string.IsNullOrEmpty(this.ToolTip))
                this.ToolTip = "جدول";
            base.Render(writer); 
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;
            const string jsResource = "ITC.Library.Scripts.CustomeRadGridView.CustomeRadGridView.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }
            scriptLoaded = true;
        }


        protected override void OnInit(EventArgs e)
        {
            var CustomeClientStr =  CustomeClientDataKeyId + "," + CustomeClientDataTitle + "," + CustomeTitle ;
            string[] dataKeyes = CustomeClientStr.Split(',');

            for (int i = 0; i < dataKeyes.Length; i++)
            {
                Page.ClientScript.RegisterArrayDeclaration("Skills", "'" + dataKeyes[i] + "'");
            }
            this.ClientSettings.ClientEvents.OnRowSelected = "RowSelected";
        }

        //protected override void CreateChildControls()
        //{
            
            //this.AllowCustomPaging = true;
            //this.AllowPaging = true;
            //this.AllowSorting = true;
            //this.ShowFooter = true;
            //this.AutoGenerateColumns = false;
            //this.CellSpacing = 0;
            //this.GridLines = GridLines.None;
            //this.HorizontalAlign = HorizontalAlign.Center;
            //this.Skin = "Office2007";
            //this.MasterTableView.Dir = GridTableTextDirection.RTL;
            //this.MasterTableView.NoMasterRecordsText = "اطلاعاتی برای نمایش یافت نشد";
            //this.MasterTableView.GroupsDefaultExpanded = false;
            //this.MasterTableView.PagerStyle.AlwaysVisible = true;
            //this.MasterTableView.PagerStyle.AlwaysVisible = true;
            //this.MasterTableView.PagerStyle.FirstPageToolTip = "صفحه اول";
            //this.MasterTableView.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
            //this.MasterTableView.PagerStyle.LastPageToolTip = "صفحه آخر";
            //this.MasterTableView.PagerStyle.NextPagesToolTip = "صفحات بعدی";
            //this.MasterTableView.PagerStyle.NextPageToolTip = "صفحه بعدی";
            //this.MasterTableView.PagerStyle.PageSizeLabelText = "اندازه صفحه";
            //this.MasterTableView.PagerStyle.PrevPagesToolTip = "صفحات قبلی";
            //this.MasterTableView.PagerStyle.PrevPageToolTip = "صفحه قبلی";
            //this.MasterTableView.PagerStyle.VerticalAlign = VerticalAlign.Middle;
            //this.StatusBarSettings.LoadingText = "بارگذاری...";
            //this.StatusBarSettings.ReadyText = "آماده";
            //this.ClientSettings.Selecting.AllowRowSelect = true;
            //this.MasterTableView.ClientDataKeyNames = dataKeyes; 


            
        //}

    }
}
