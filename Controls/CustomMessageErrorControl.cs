﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;


[assembly: System.Web.UI.WebResource("ITC.Library.Images.MessageErrorControl.Error.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.MessageErrorControl.Ok.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.MessageErrorControl.Warning.png", "image/png")]
namespace ITC.Library.Controls
{
   public class CustomMessageErrorControl :WebControl
   {
       private string imageName;
       private Label MessageLabel;
       private Image MessageImage;
       protected override void Render(System.Web.UI.HtmlTextWriter writer)
       {

           var Htmlsource = "";    
           base.Render(writer);   
       }


       protected override void OnInit(EventArgs e)
       {
           MessageImage = new Image(); 
           MessageImage.Height = 15;          
           this.Controls.Add(MessageImage);
           MessageLabel = new Label();           
           MessageLabel.ID = this.ClientID + "_txtTitle";
           MessageLabel.Height = 15;
           MessageLabel.Width = this.Width;
           MessageLabel.Font.Name = "Arial";
           MessageLabel.Font.Size = 10;
           MessageLabel.BorderWidth = 0;
           this.Controls.Add(MessageLabel);
       }

       protected override void OnLoad(EventArgs e)
       {
           MessageImage.Visible = false;
           MessageLabel.Visible = false;
       }

       public void ShowErrorMessage(string MessageText)
       {
           MessageImage.Visible = true;
           MessageLabel.Visible = true;
           MessageLabel.ForeColor = System.Drawing.Color.Red;
           MessageLabel.Text = MessageText;
           MessageImage.ImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.MessageErrorControl.Error.png");
       }
       public void ShowSuccesMessage(string MessageText)
       {           
           MessageImage.Visible = true;
           MessageLabel.Visible = true;
           MessageLabel.ForeColor = System.Drawing.Color.Green;
           MessageLabel.Text = MessageText;
           MessageImage.ImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.MessageErrorControl.Ok.png");
       }
       public void ShowWarningMessage(string MessageText)
       {           
           MessageImage.Visible = true;
           MessageLabel.Visible = true;
           MessageLabel.Text = MessageText;
           MessageLabel.ForeColor = System.Drawing.Color.DarkOrange;
           MessageImage.ImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.MessageErrorControl.Warning.png");
       }

    }
}
