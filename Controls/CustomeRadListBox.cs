﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI;
[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.CustomeRadListBox.CustomeRadListBox.js", "text/javascript", PerformSubstitution = true)]

namespace ITC.Library.Controls
{    
    public class CustomeRadListBox : RadListBox
    {
        private bool scriptLoaded = false;
        public bool ActiveSelectAll { get; set; }
        private HiddenField hiddenfieldSelectvalue = new HiddenField();
        private HiddenField hiddenfieldGeneralValue = new HiddenField();
        public HiddenField HiddenFieldTitle = new HiddenField();
        public string KeyId
        {
            get { return hiddenfieldSelectvalue.Value; }
            set { hiddenfieldSelectvalue.Value = value; }
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            var Htmlsource =
                string.Format("<table width='{1}'>" +
                "<tr><td width='{1}'><input id='{0}_txtSearch' type='text'  onblur='SetTitleSearch(this);' onfocus='ClearTitleSearch(this);' onkeyup='SearchINListBox(this,{0});' style='width: 100%';  value='جستجو' /></td></tr>" +
                SetShowCheckBox() + "</table>", this.ClientID, this.Width);
            writer.Write(Htmlsource);
            hiddenfieldSelectvalue.RenderControl(writer);
            hiddenfieldGeneralValue.RenderControl(writer);
            HiddenFieldTitle.RenderControl(writer);
            base.Render(writer);

        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;
            const string jsResource = "ITC.Library.Scripts.CustomeRadListBox.CustomeRadListBox.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }
            scriptLoaded = true;
        }

        string SetShowCheckBox()
        {
            string TagChecbox = "";
            if (ActiveSelectAll)
                TagChecbox = string.Format("<tr><td bgcolor='#EBEBEB'><input id='{0}_SelectAll' type='checkbox'  value='انتخاب همه' title='انتخاب همه'  onclick='SelectDeSelectAll(this,{0})'   />انتخاب همه</td></tr>", this.ClientID);
            return TagChecbox;
        }


        protected override void OnInit(EventArgs e)
        {
            hiddenfieldSelectvalue = new HiddenField();
            hiddenfieldSelectvalue.ID = this.ClientID + "_hiddenfieldSelectKeyId";
            this.Controls.Add(hiddenfieldSelectvalue);
            this.OnClientLoad = "LoadListBox";
            this.OnClientItemChecked = "OnClientItemCheckedHandler";
            this.CheckBoxes = (ActiveSelectAll == true ? true : false);

            hiddenfieldGeneralValue = new HiddenField();
            hiddenfieldGeneralValue.ID = this.ClientID + "_hiddenfieldGeneralKeyId";
            this.Controls.Add(hiddenfieldGeneralValue);

            HiddenFieldTitle.ID = this.ClientID + "_hiddenfieldTitle";
            this.Controls.Add(HiddenFieldTitle);
        }



    }
}