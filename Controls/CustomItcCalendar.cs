﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.CustomItcCalendar.CustomItcCalendar.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.JS.jquery.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Styles.CustomItcCalendar.CustomItcCalendar.css", "text/css", PerformSubstitution = true)]
namespace ITC.Library.Controls
{
    public class CustomItcCalendar : TextBox
    {
        private bool scriptLoaded = false;
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(this.ToolTip))
                this.ToolTip = "تقویم";
            this.Width = 65;
            base.Render(writer);
            var StrCalandarImage = string.Format(@"<img ID='{0}_ItcCalandarImageButton'  src='{1}' title='{2}' style='vertical-align: top;'  Onclick=OpenDate('{3}',event);>", this.ClientID, Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.JalaliCalendar.Calendar.png"), this.ToolTip, this.ClientID);            
            writer.Write(StrCalandarImage);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;            
            this.Attributes.Add("onblur", "DateValidation(this);");            
            const string jsResource = "ITC.Library.Scripts.CustomItcCalendar.CustomItcCalendar.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }
            const string jqueryResource = "ITC.Library.JS.jquery.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jqueryResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jqueryResource);
            }
            const string styleResource = "ITC.Library.Styles.CustomItcCalendar.CustomItcCalendar.css";
            var csslink = "<link rel='stylesheet' type='text/css' href='" + Page.ClientScript.GetWebResourceUrl(this.GetType(), styleResource) + "' />";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(csslink))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "StyleSheet1", csslink);
            }

            scriptLoaded = true;

        }

    }
}
