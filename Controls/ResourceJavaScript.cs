﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: TagPrefix("ITC.Controls", "asp")]
//Resource JavaScript  JalaliCalendar
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.JalaliCalendar.DatetimePicker.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.JalaliCalendar.Calendar.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Styles.JalaliCalendar.DateTimePicker.css", "text/css", PerformSubstitution = true)]
//Resource JavaScript  SelectControl
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.SelectControl.SelectControl.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Person.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.OrganizationPhysicalChart.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Organization.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Upload.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.PersonAdd.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Bullets.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Post.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Members.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.MemberShip.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Action.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Send.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.SelectPerson.png", "image/png")]

//Resource JavaScript  RadComboBox
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.RadComboBox.RadComboBox.js", "text/javascript", PerformSubstitution = true)]
//Resource JavaScript  NumericTextBox
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.NumericTextBox.NumericTextBox.js", "text/javascript", PerformSubstitution = true)]
//Resource JavaScript  CustomeRadListBox
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.CustomeRadListBox.CustomeRadListBox.js", "text/javascript", PerformSubstitution = true)]
//Resource JavaScript  CustomeRadButton
[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.RollBackCommandment.png", "image/png")]
//Resource JavaScript  CustomItcCalendar
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.CustomItcCalendar.CustomItcCalendar.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.JalaliCalendar.Calendar.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Styles.CustomItcCalendar.CustomItcCalendar.css", "text/css", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.JS.jquery.js", "text/javascript", PerformSubstitution = true)]
namespace ITC.Library.Controls
{

  public  class ResourceJavaScript:WebControl
    {
      private bool scriptLoaded = false;

      protected override void OnPreRender(EventArgs e)
      {
          base.OnPreRender(e);
          if (scriptLoaded) return;

          //To add the JS file to the custom control
          const string jsJalaliCalendarResource = "ITC.Library.Scripts.JalaliCalendar.DatetimePicker.js";
          const string styleJalaliCalendarResource = "ITC.Library.Styles.JalaliCalendar.DateTimePicker.css";
          const string jsSelectControlResource = "ITC.Library.Scripts.SelectControl.SelectControl.js";
          const string jsRadComboBoxResource = "ITC.Library.Scripts.RadComboBox.RadComboBox.js";
          const string jsNumericTextBoxResource = "ITC.Library.Scripts.NumericTextBox.NumericTextBox.js";
          const string jsCustomeRadListBox = "ITC.Library.Scripts.CustomeRadListBox.CustomeRadListBox.js";
          const string jsCustomItcCalendar = "ITC.Library.Scripts.CustomItcCalendar.CustomItcCalendar.js";
          const string styleCustomItcCalendar = "ITC.Library.Styles.CustomItcCalendar.CustomItcCalendar.css";
          const string jquery = "ITC.Library.JS.jquery.js";

   
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsJalaliCalendarResource);
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsSelectControlResource);
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsRadComboBoxResource);
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsNumericTextBoxResource);
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsCustomeRadListBox);
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsCustomItcCalendar);
              this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jquery);


              var csslink = "";
              var cssstyleJalaliCalendarResourcelink = "<link rel='stylesheet' type='text/css' href='" + Page.ClientScript.GetWebResourceUrl(this.GetType(), styleJalaliCalendarResource) + "' />";
              var cssstyleCustomItcCalendarlink = "<link rel='stylesheet' type='text/css' href='" + Page.ClientScript.GetWebResourceUrl(this.GetType(), styleCustomItcCalendar) + "' />";
              csslink = cssstyleCustomItcCalendarlink + cssstyleJalaliCalendarResourcelink;
              if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(csslink))
               {
              this.Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "StyleSheet", csslink);
               }
              scriptLoaded = true;


          //var csslink1 = "<link rel='stylesheet1' type='text/css' href='" + Page.ClientScript.GetWebResourceUrl(this.GetType(), styleCustomItcCalendar) + "' />";
          //if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(csslink1))
          //{
          //    this.Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "StyleSheet1", csslink1);
          //}


      }
    }
}
