using System;
using System.Web.UI;

[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.NumericTextBox.NumericTextBox.js", "text/javascript", PerformSubstitution = true)]

namespace ITC.Library.Controls
{
    [ToolboxData(@"<{0}:NumericTextBox Text="""" runat=""server"" ></{0}:NumericTextBox>")]
    public partial class NumericTextBox : System.Web.UI.WebControls.TextBox
    {
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            ClientScriptManager scriptManager = this.Page.ClientScript;
            string resourceFilePath = "ITC.Library.Scripts.NumericTextBox.NumericTextBox.js";

            // This will register a Javascript block witht the name 'NumericTextBoxScript'
            //scriptManager.RegisterClientScriptInclude("NumericTextBoxScript", scriptManager.GetWebResourceUrl(this.GetType(), resourceFilePath));
            this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), resourceFilePath);
            if (this.Type == TextBoxType.Decimal)
                this.Attributes.Add("onkeydown", string.Format("return CheckDecimal(this,'{0}','{1}', {2})", NumberOfInteger, NumberOfFraction, _AllowNegative.ToString().ToLower()));
            else if (this.Type == TextBoxType.Integer)
                this.Attributes.Add("onkeydown", string.Format("return CheckInteger({0})", _AllowNegative.ToString().ToLower()));

            this.Attributes.Add("onkeyup", string.Format("return CheckNegative(this)", _AllowNegative.ToString().ToLower()));
        }
    }
}
