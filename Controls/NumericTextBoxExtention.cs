
namespace ITC.Library.Controls
{
    public partial class NumericTextBox : System.Web.UI.WebControls.TextBox
    {
        public enum TextBoxType
        {
            Integer,
            Decimal
        }

        private int _NumberOfFraction = 0;
        private int _NumberOfInteger = 0;
        private bool _AllowNegative = false;
        private TextBoxType _Type = TextBoxType.Integer;

        public int NumberOfFraction
        {
            get { return _NumberOfFraction; }
            set { _NumberOfFraction = value; }
        }

        public int NumberOfInteger
        {
            get { return _NumberOfInteger; }
            set { _NumberOfInteger = value; }
        }

        public bool AllowNegative
        {
            get { return _AllowNegative; }
            set { _AllowNegative = value; }
        }

        public TextBoxType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }        
        
    }
}