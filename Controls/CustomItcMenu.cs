﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.Configuration.Settings;
using Intranet.Security;
using Telerik.Web.UI;

namespace ITC.Library.Controls
{
   public class CustomItcMenu:RadPanelBar
   {

       public string MenuTableName { get; set; }
       public string MenuImageUrl { get; set; }
       private int ModulId;
       private string UserId;
       //public string PageName;
       //public string IsLoadControl;

       protected override void Render(System.Web.UI.HtmlTextWriter writer)
       {
           this.ToolTip = "منو اصلی";
           this.CausesValidation = false;     
           this.ExpandMode=PanelBarExpandMode.SingleExpandedItem;
           base.Render(writer);
       }
       public void LoadMenuItc()
       {
           this.CausesValidation = false;
           ModulId = FindMouleId();
           UserId = HttpContext.Current.User.Identity.Name;
           DataTable dataTable = new DataTable();
           var SqlConnection = ITC.Library.Classes.ItcDatabaseManager.GetDbManager();
           SqlCommand sqlCommand = new SqlCommand();
           sqlCommand.CommandType = CommandType.StoredProcedure;
           sqlCommand.CommandText = "[pub].[p_PubItcMenuRootLevelLoad]";
           sqlCommand.Parameters.Add("@ItcMenuModulId", SqlDbType.Int);
           sqlCommand.Parameters.Add("@MenuTableName", SqlDbType.NVarChar, 150);
           sqlCommand.Parameters.Add("@UserId", SqlDbType.NVarChar, 50);
           sqlCommand.Parameters["@ItcMenuModulId"].Value = ModulId;
           sqlCommand.Parameters["@MenuTableName"].Value = MenuTableName;
           sqlCommand.Parameters["@UserId"].Value = UserId;
           dataTable = SqlConnection.GetTable(sqlCommand);
           PopulateNodes(dataTable, this.Items);
           HttpContext.Current.Session["ModuleId"] = FindMouleId();
       }
       private void PopulateSubLevel(int ItcMenuOwnerId, int ModulId, string UserId, RadPanelItem radPanelItem)
       {
           DataTable dataTable = new DataTable();
           var SqlConnection = ITC.Library.Classes.ItcDatabaseManager.GetDbManager();
           SqlCommand sqlCommand = new SqlCommand();
           sqlCommand.CommandType = CommandType.StoredProcedure;
           sqlCommand.CommandText = "[pub].[p_PubItcMenuSubLevelLoad]";
           sqlCommand.Parameters.Add("@ItcMenuModulId", SqlDbType.Int);
           sqlCommand.Parameters.Add("@MenuTableName", SqlDbType.NVarChar, 150);
           sqlCommand.Parameters.Add("@UserId", SqlDbType.NVarChar, 50);
           sqlCommand.Parameters.Add("@ItcMenuOwnerId", SqlDbType.Int);
           sqlCommand.Parameters["@ItcMenuModulId"].Value = ModulId;
           sqlCommand.Parameters["@MenuTableName"].Value = MenuTableName;
           sqlCommand.Parameters["@UserId"].Value = UserId;
           sqlCommand.Parameters["@ItcMenuOwnerId"].Value = ItcMenuOwnerId;
           dataTable = SqlConnection.GetTable(sqlCommand);
           PopulateNodes(dataTable, radPanelItem.Items);
       }
       private void PopulateNodes(DataTable dataTable, RadPanelItemCollection radPanelItemCollection)
       {
           foreach (DataRow dr in dataTable.Rows)
           {
               RadPanelItem radPanelItem = new RadPanelItem();              
               radPanelItem.Text = dr["ItcMenuTitle"].ToString();
               if (((int) dr["ChildCount"]) > 0)
               {
                  radPanelItem.PostBack = false;  
               }
              
               //radPanelItem.ImageUrl = (dr["ItcMenuImageUrl"].ToString() == "" ? dr["ItcMenuImageUrl"].ToString() : PortalSettings.PortalPath + "/DeskTopModules/" + MenuImageUrl+"/" + dr["ItcMenuImageUrl"].ToString());
               radPanelItem.ImageUrl = (dr["ItcMenuImageUrl"].ToString() == "" ? dr["ItcMenuImageUrl"].ToString() : PortalSettings.PortalPath + "/DeskTopModules/"   + dr["ItcMenuImageUrl"].ToString());
               radPanelItem.Font.Size = new FontUnit((dr["ItcMenuTextSize"].ToString()));
               radPanelItem.Font.Bold = ((dr["ItcMenuIsBoldText"].ToString()) == "" ? false : bool.Parse(dr["ItcMenuIsBoldText"].ToString()));
               radPanelItem.Font.Name = (String.IsNullOrEmpty(dr["ItcMenuFontName"].ToString()) ? "Tahoma" : dr["ItcMenuFontName"].ToString());
               var ItcMenuParameter = new ItcMenuParameter();
               ItcMenuParameter.PageName = dr["ItcMenuPageName"].ToString();
               ItcMenuParameter.IsLoadControl = bool.Parse(dr["ItcMenuIsLoadControl"].ToString());
               radPanelItem.Value = Classes.JsonExecutor.SerializeCustomData(ItcMenuParameter);               
               Color ForColor = Color.FromName(dr["ItcMenuColorName"].ToString());
               radPanelItem.ForeColor = ForColor;
               radPanelItemCollection.Add(radPanelItem);
               if ((int)(dr["ChildCount"]) > 0)
               {
                   PopulateSubLevel(Convert.ToInt32(dr["ItcMenuId"]), ModulId, UserId, radPanelItem);
               }
           }
       }
       private int FindMouleId()
       {
           int ModuleId = 0;
           string Url = HttpContext.Current.Request.Url.ToString();
           if (Url.ToLower().Contains("mid="))
           {
               ModuleId = Int32.Parse(HttpContext.Current.Request.QueryString["mid"]);
           }
           else
           {
               int Start = Url.IndexOf("-");
               ModuleId = int.Parse(Url.Substring(Start + 1, 4));
           }

           return ModuleId;
       }




   }
}
