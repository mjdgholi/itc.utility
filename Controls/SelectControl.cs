﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using Intranet.Configuration.Settings;
using Telerik.Pdf;
using Telerik.Web.UI;

[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.SelectControl.SelectControl.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Person.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.OrganizationPhysicalChart.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Organization.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.upload.jpg", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.PersonAdd.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Bullets.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Post.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.ClearSelectControl.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Calendar.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Editor.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Members.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.MemberShip.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.RunQuery.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Add.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Action.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Send.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.SelectPerson.png", "image/png")]


namespace ITC.Library.Controls
{
   [
   ToolboxData(@"<{0}:SelectControl runat=""server""> </{0}:SelectControl>")
   ]
    public class SelectControl : WebControl
    {
        private bool scriptLoaded = false;
        private TextBox txtKeyId;
        public string KeyId
        {
            get { return txtKeyId.Text; }
            set { txtKeyId.Text = value; }
        }
        private TextBox txtTitleHidden;
        private TextBox txtTitle;
        public string PortalPathUrl { get; set; }
        public string Title
        {
            get { return txtTitle.Text; }
            set { txtTitle.Text = value; }
        } 
        public bool FilterByModulId { get; set; }
        public string WhereClause { get { return (ViewState["ItcSelectControlWhereClause"] != null) ? ViewState["ItcSelectControlWhereClause"].ToString() : ""; } set { ViewState["ItcSelectControlWhereClause"] = value; } }
        public enum enumimageName
        {
            Person,
            OrganizationPhysicalChart,
            Organization,
            Upload,
            PersonAdd,
            Bullets,
            Post,
            Calendar,
            Editor,
            Members,
            MemberShip,
            RunQuery, 
            Add,
            Action,
            Send,
            SelectPerson
        }
        public enumimageName imageName { get; set; }
        private string StrParameter = "";
        public int RadWindowWidth { get; set; }
        public int RadWindowHeight { get; set; }
        public bool ActiveCloseRadWindow { get; set; }
        public bool PrsOrganizationPhysicalChartFullAccess { get; set; }
        public bool SelectControlClear { get; set; }
        public bool HasNationalNo { get; set; }
        public bool MultiSelect { get; set; }
        private RadWindowManager RadWindowManager;
        private RadWindow Radwindow;
        public string FunctionJsDeveloperName { get; set; }
        public string FunctionJsDeveloperNameinOnClientClose { get; set; }
       public string InitialBehavior { get; set; }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            var Htmlsource = "";
            base.Render(writer);
            if(SelectControlClear)
                Htmlsource = string.Format(@"<img ID='{0}_PersonSelectControlImageButton'   src='{1}' title='{2}'   style= ' height: 25px ; cursor: pointer;vertical-align: top; border-style: solid; border-width: 1px; border-color: #C0C0C0 ; background-color: #FFFFFF ' Onclick=""" +
                               GetJSFunctionTotitle() + " \"><img ID='{0}_ClearSelectControlImageButton'   src='{3}' title='{4}'   style= ' height: 25px ; cursor: pointer;vertical-align: top; border-style: solid; border-width: 1px; border-color: #C0C0C0 ; background-color: #FFFFFF ' onclick=" +GetJSFunctionCleartitle() + " />"
                               , this.ClientID, Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.SelectControl." + imageName + ".png"), this.ToolTip, Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.SelectControl.ClearSelectControl.png"), "پاک کردن");
            else
                Htmlsource = string.Format(@"<img ID='{0}_PersonSelectControlImageButton'   src='{1}' title='{2}'   style= ' height: 25px ; cursor: pointer;vertical-align: top; border-style: solid; border-width: 1px; border-color: #C0C0C0 ; background-color: #FFFFFF ' Onclick=""" +
                                 GetJSFunctionTotitle() + " \">"
                                 , this.ClientID, Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.SelectControl." + imageName + ".png"), this.ToolTip);
            
            writer.Write(Htmlsource);
        }

        private string GetJSFunctionTotitle()
        {
            var str="";
            if (this.Enabled == false)
                str = "";
            else
            {

                var StrTitlePortalPath = PortalSettings.PortalPath + "/DeskTopModules/" + PortalPathUrl; ;                
                var KeyIdtxtObj = txtKeyId.ClientID;
                var TitleTxtObj = txtTitle.ClientID;
                var TitletxtHiddenobj = txtTitleHidden.ClientID;
                var SetFilterByModuleId = (FilterByModulId ? 1 : 0);
                var SetWhereClause = WhereClause;
                var SetActiveCloseRadWindow = (ActiveCloseRadWindow ? 1 : 0);                              
                var SetPrsOrganizationPhysicalChartFullAccess = (PrsOrganizationPhysicalChartFullAccess ? 1 : 0);
                var RadWindowObj =Radwindow.ClientID;
                var SetHasNationalNo = (HasNationalNo?1:0);
                var SetMultiSelect = (MultiSelect ? 1 : 0);
                StrParameter = (StrParameter.Replace("{", "{{")).Replace("}", "}}");
                StrParameter = (StrParameter != "" ? StrParameter.Replace(@"""", "&quot;") : "");
                str = string.Format("ShowRadWindow('{0}','{1}','{2}','{3}',{4},'{5}',{6},'{7}',{8},'{9}',{10},'{11}','{12}','{13}','{14}',{15});", StrTitlePortalPath, TitleTxtObj, TitletxtHiddenobj, KeyIdtxtObj, SetFilterByModuleId, SetWhereClause, SetActiveCloseRadWindow, SetPrsOrganizationPhysicalChartFullAccess, RadWindowObj, StrParameter,SetHasNationalNo,KeyId,Title,FunctionJsDeveloperName,FunctionJsDeveloperNameinOnClientClose,SetMultiSelect);
            }
        return str; 
        }

        private string GetJSFunctionCleartitle()
        {
 
            var str="";  
                var KeyIdtxtObj = txtKeyId.ClientID;
                var TitleTxtObj = txtTitle.ClientID;
                var TitletxtHiddenobj = txtTitleHidden.ClientID;
                str = string.Format("ClearSelectControlText('{0}','{1}','{2}');", TitleTxtObj, TitletxtHiddenobj, KeyIdtxtObj);
    
            return str;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;
            const string jsResource = "ITC.Library.Scripts.SelectControl.SelectControl.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }
            scriptLoaded = true;
        }




        protected override void OnInit(EventArgs e)
        {
            RadWindowManager = new RadWindowManager();
            RadWindowManager.ID = this.ClientID + "RadWindowManager1";
            RadWindowManager.ShowContentDuringLoad = false;
            RadWindowManager.ReloadOnShow = true;
            RadWindowManager.EnableShadow = true;
            
       
            //RadWindowManager.Skin = "WebBlue";
            RadWindowManager.Width = (RadWindowWidth == 0 ? 850 : RadWindowWidth);
            RadWindowManager.Height = (RadWindowHeight == 0 ? 520 : RadWindowHeight);
            RadWindowManager.OnClientClose = "OnClientClose";
            RadWindowManager.Behaviors = WindowBehaviors.Close | WindowBehaviors.Maximize|WindowBehaviors.Move;

            

            RadWindowManager.VisibleStatusbar = false;
            RadWindowManager.Modal = true;
            Controls.Add(RadWindowManager);

            Radwindow = new RadWindow();
            Radwindow.ID = this.ClientID + "Radwindow";
            Radwindow.Title = this.ToolTip;   
            RadWindowManager.Windows.Add(Radwindow);
            //Radwindow.Behavior=WindowBehaviors.Move;
       

            txtTitle = new TextBox();
            txtTitle.ID = this.ClientID + "_txtTitle";
           // var unit = new Unit(this.Height.Value + 2);
            //var height = (Convert.ToInt32(this.Height)+2).ToString();
            //var unit = new Unit(this.Height.Value+2);
            txtTitle.Height = 27;
            txtTitle.Visible = (this.Width == 0 ? false : true);
           
            txtTitle.Width = this.Width;

            txtTitle.Font.Name = "Arial";
            txtTitle.Font.Size = 10;
            txtTitle.ForeColor = System.Drawing.Color.FromName("#000099");


            txtTitle.BorderWidth = 1;
            txtTitle.BorderColor = System.Drawing.Color.FromName("#C0C0C0");

            //txtTitle.ReadOnly = true;
            txtTitle.Attributes.Add("readonly", "readonly");
            this.Controls.Add(txtTitle);
            
            

            txtTitleHidden = new TextBox(){Width ='0',Height = '0'};
            txtTitleHidden.ID = this.ClientID + "_txtTitleHidden";
            txtTitleHidden.Attributes.Add("style", "display:none");
            this.Controls.Add(txtTitleHidden);
            txtTitleHidden.TextChanged += new EventHandler(txtTitleHidden_TextChanged);

                                          
            txtKeyId = new TextBox() { Width = 0, Height = 0 };
            txtKeyId.ID = this.ClientID + "_KeyIdtxt";
            Controls.Add(txtKeyId);
            txtKeyId.Attributes.Add("style", "display:none");      
        }

        


        void txtTitleHidden_TextChanged(object sender, EventArgs e)
        {
            Title = txtTitleHidden.Text;

        }


        public void ParameterPost(object obj)
        {
            StrParameter = ITC.Library.Classes.JsonExecutor.SerializeCustomData(obj);
        }


    }
}
