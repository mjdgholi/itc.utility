﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Web.UI;

[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.Excel.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.Word.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.Pdf.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.CloseDate.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.RollBackCommandment.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.RadButtonImage.Calculate.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectAll.Selectall.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectPersonnelAndBack.SelectPersonnelAndBack.png", "image/png")]
namespace ITC.Library.Controls
{
   public class CustomRadButton:RadButton
    {

       public enum enumimageButtonType
        {
          Add,
          Edit,
          Search,
          ShowAll,
          Cancel,
          Print,
          ExcelExport,
          WordExport,
          PdfExport,
          Back,
          Delete,
          Calculate,
          SelectAll,
          SelectPersonnelAndBack,
          CloseDate,
          RollBackCommandment,

        }
       public enumimageButtonType CustomeButtonType { get; set; }

       protected override void Render(System.Web.UI.HtmlTextWriter writer)
       {
           this.Width = (this.Width==System.Web.UI.WebControls.Unit.Empty ? 100 : this.Width);
           //this.Skin = "default";       
           switch (CustomeButtonType)
           {
               case enumimageButtonType.Add:
                   this.Text = "افزودن";
                   this.ToolTip = "افزودن";
                   this.Icon.PrimaryIconCssClass = "rbAdd";
                   break;
               case enumimageButtonType.Edit:
                   this.Text = "پذیرش";
                   this.ToolTip = "پذیرش";
                   this.Icon.PrimaryIconCssClass = "rbEdit";
                   break;
         
               case enumimageButtonType.Search:
                   this.Text = "جستجو";
                   this.ToolTip = "جستجو";
                   this.Icon.PrimaryIconCssClass = "rbSearch";
                   break;
               case enumimageButtonType.ShowAll:
                   this.Text = "نمایش همگی";
                   this.ToolTip = "نمایش همگی";
                   this.Icon.PrimaryIconCssClass = "rbRefresh";
                   break;
               case enumimageButtonType.Cancel:
                   this.Text = "انصراف";
                   this.ToolTip = "انصراف";
                   this.Icon.PrimaryIconCssClass = "rbCancel";
                   break;
               case enumimageButtonType.Print:
                   this.Text = (this.Text==""?"چاپ":this.Text);
                   this.ToolTip = (this.Text == "" ? "چاپ" : this.Text);
                   this.Icon.PrimaryIconCssClass = "rbPrint";
                   break;
                 case enumimageButtonType.ExcelExport:
                   this.Text = (this.Text==""?"خروجی Excel":this.Text);
                   this.ToolTip = (this.Text == "" ? "خروجی Excel" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                    this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.RadButtonImage.Excel.png");
                   break;
                 case enumimageButtonType.WordExport:
                   this.Text = (this.Text==""?"خروجی Word":this.Text);;
                   this.ToolTip = (this.Text==""?"خروجی Word":this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.RadButtonImage.Word.png");
                   break;
                 case enumimageButtonType.PdfExport:
                   this.Text = (this.Text == "" ? "خروجی PDF" : this.Text); ;
                   this.ToolTip = (this.Text == "" ? "خروجی PDF" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.RadButtonImage.Pdf.png");
                   break;
               case enumimageButtonType.Back:
                   this.Text = "بازگشت";
                   this.ToolTip = "بازگشت";
                   this.Icon.PrimaryIconCssClass = "rbPrevious";
                   break;
               case enumimageButtonType.Delete:
                   this.Text = "حذف";
                   this.ToolTip = "حذف";
                   this.Icon.PrimaryIconCssClass = "rbRemove";
                   break;
               case enumimageButtonType.SelectAll:
                   this.Text = (this.Text == "" ? "انتخاب همه" : this.Text); ;
                   this.ToolTip = (this.Text == "" ? "انتخاب همه" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.SelectAll.Selectall.png");
                   break;
               case enumimageButtonType.SelectPersonnelAndBack:
                   this.Text = (this.Text == "" ? "انتخاب و بازگشت" : this.Text); ;
                   this.ToolTip = (this.Text == "" ? "انتخاب و بازگشت" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.SelectPersonnelAndBack.SelectPersonnelAndBack.png");
                   break;
               case enumimageButtonType.CloseDate:
                   this.Text = (this.Text == "" ? "بستن بازه" : this.Text); ;
                   this.ToolTip = (this.Text == "" ? "بستن بازه" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.RadButtonImage.CloseDate.png");
                   break;
               case enumimageButtonType.RollBackCommandment:
                   this.Text = (this.Text == "" ? "برگشت حکم" : this.Text); ;
                   this.ToolTip = (this.Text == "" ? "برگشت حکم" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.RadButtonImage.RollBackCommandment.png");
                   break;
               case enumimageButtonType.Calculate:
                   this.Text = (this.Text == "" ? "محاسبه" : this.Text); ;
                   this.ToolTip = (this.Text == "" ? "محاسبه" : this.Text);
                   this.Icon.PrimaryIconCssClass = "";
                   this.Icon.PrimaryIconUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.RadButtonImage.Calculate.png");
                   break;
           }
          
           base.Render(writer);
        
       }


    }
}
