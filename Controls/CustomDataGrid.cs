﻿using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

[assembly: TagPrefix("ITC.Controls", "asp")]
namespace ITC.Library.Controls
{
    [
        ToolboxData(@"<{0}:CustomDataGrid runat=""server""> </{0}:CustomDataGrid>")
    ]
    public class CustomDataGrid : GridView
    {
            private const string VirtualCountItemViewName = "bg_vitemCount";
            private const string SortColumnViewName = "bg_sortColumn";
            private const string SortDirectionViewName = "bg_sortDirection";
            private const string CurrentPageIndexViewName = "bg_pageIndex";


            #region Custom Properties
            [Browsable(true), Category("Paging")]
            [Description("Set the virtual item count for this grid")]
            public int VirtualItemCount
            {
                get
                {
                    if (ViewState[VirtualCountItemViewName] == null)
                        ViewState[VirtualCountItemViewName] = -1;
                 
                    return Convert.ToInt32(ViewState[VirtualCountItemViewName]);
                }
                set
                {
                    ViewState[VirtualCountItemViewName] = value;
                }
            }

            public string GridViewSortColumn
            {
                get
                {
                    if (ViewState[SortColumnViewName] == null)
                        ViewState[SortColumnViewName] = string.Empty;
                    return ViewState[SortColumnViewName].ToString();
                }
                set
                {
                    if (ViewState[SortColumnViewName] == null || !ViewState[SortColumnViewName].Equals(value))
                        GridViewSortDirection = SortDirection.Ascending;
                    ViewState[SortColumnViewName] = value;
                }
            }

            public SortDirection GridViewSortDirection
            {
                get
                {
                    if (ViewState[SortDirectionViewName] == null)
                        ViewState[SortDirectionViewName] = SortDirection.Ascending;
                    return (SortDirection)ViewState[SortDirectionViewName];
                }
                set
                {
                    ViewState[SortDirectionViewName] = value;
                }
            }

            private int CurrentPageIndex
            {
                get
                {
                    if (ViewState[CurrentPageIndexViewName] == null)
                        ViewState[CurrentPageIndexViewName] = 0;
                    return Convert.ToInt32(ViewState[CurrentPageIndexViewName]);
                }
                set
                {
                    ViewState[CurrentPageIndexViewName] = value;
                }
            }
            public override int PageIndex
            {
                get
                {
                    return CurrentPageIndex;
                }
                set
                {
                    CurrentPageIndex = value;
                }
            }

            private bool CustomPaging
            {
                get { return (VirtualItemCount != -1); }
            }


            //public CssStyleCollection mySelectedRowStyle
            //{
            //    get
            //    {
            //        return mySelectedRowStyle;
            //    }
            //    set
            //    {
            //        mySelectedRowStyle = value;
            //    }
            //}
            //public System.Web.UI.WebControls.DataKey SelectedRowDataKey
            //{
            //    get
            //    {
            //        return SelectedRowDataKey;
            //    }
            //    set
            //    {
            //        SelectedRowDataKey = value;
            //    }
            //}
           #endregion

            #region Overriding the parent methods
            public override object DataSource
            {
                get
                {
                    return base.DataSource;
                }
                set
                {
                    base.DataSource = value;
                    CurrentPageIndex = PageIndex;
                }
            }

            protected override void OnSorting(GridViewSortEventArgs e)
            {
                SortDirection direction = SortDirection.Ascending;
                if (ViewState[SortColumnViewName] != null && (SortDirection)ViewState[SortDirectionViewName] == SortDirection.Ascending)
                {
                    direction = SortDirection.Descending;
                }
                GridViewSortDirection = direction;
                GridViewSortColumn = e.SortExpression;
                base.OnSorting(e);
            }

            protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
            {
                // This method is called to initialise the pager on the grid. We intercepted this and override
                // the values of pagedDataSource to achieve the custom paging using the default pager supplied
                if (CustomPaging)
                {
                    pagedDataSource.AllowCustomPaging = true;
                    pagedDataSource.VirtualCount = VirtualItemCount;
                    pagedDataSource.CurrentPageIndex = CurrentPageIndex;
                }
                base.InitializePager(row, columnSpan, pagedDataSource);
            }

            //protected override void OnRowDataBound(GridViewRowEventArgs e)
            //{
            //    base.OnRowDataBound(e);
            //    //if (this.DataKeys[e.Row.RowIndex] == SelectedRowDataKey)
            //    //    e.Row.Style = this.mySelectedRowStyle;

            //}

            //protected override void OnRowCreated(GridViewRowEventArgs e)
            //{
            //     base.OnRowCreated(e);
            //   if (e.Row.RowType == DataControlRowType.DataRow)
            //    {
            //        //#EFF3FB#E6ECF9
            //        e.Row.Attributes.Add("onMouseOver", "this.style.background='#E8EDF4'");

            //        e.Row.Attributes.Add("onMouseOut", "this.style.background='#ffffff';this.style.fontWeight='normal'");

            //    }
            //}
            #endregion

        }

}