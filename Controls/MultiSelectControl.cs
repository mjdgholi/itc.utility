﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using Telerik.Web.UI;
using Intranet.Configuration.Settings;

[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.MultiSelectControl.MultiSelectControl.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.SelectControl.Person.png", "image/png")]
namespace ITC.Library.Controls
{
    [
    ToolboxData(@"<{0}:MultiSelectControl runat=""server""> </{0}:MultiSelectControl>")
    ]
    public class MultiSelectControl : CustomeRadListBox
    {
        private bool scriptLoaded = false;
        private CustomeRadListBox CustomeRadListBox;
        private RadWindowManager RadWindowManager;
        private RadWindow Radwindow;
        public string PortalPathUrl { get; set; }
        public string WhereClause { get; set; }
        public bool MultiSelect { get; set; }
        public enum enumimageName
        {
            Person,
            OrganizationPhysicalChart,
            Organization,
            Upload,
            PersonAdd,
            Bullets,
        }
        public enumimageName imageName { get; set; }
        public int RadWindowWidth { get; set; }
        public int RadWindowHeight { get; set; }
        public string FunctionJsDeveloperName { get; set; }
        public string FunctionJsDeveloperNameinOnClientClose { get; set; }
        private string StrParameter = "";
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            var Htmlsource =
                string.Format("<Table width='100%'><tr><td>" +
                    @"<img ID='{0}_PersonSelectControlImageButton'   src='{1}' title='{2}'   style= ' height: 25px ; cursor: pointer;vertical-align: top; border-style: solid; border-width: 1px; border-color: #C0C0C0 ; background-color: #FFFFFF ' Onclick=""" +
                    GetJSFunctionTotitle() + " \"></td></tr></table>"
                    , this.ClientID,
                    Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.SelectControl." + imageName + ".png"), this.ToolTip);
            writer.Write(Htmlsource);
            Radwindow.RenderControl(writer);
            base.Render(writer);
        }
        private string GetJSFunctionTotitle()
        {
            var str = "";
            if (this.Enabled == false)
                str = "";
            else
            {
                var StrTitlePortalPath = PortalSettings.PortalPath + "/DeskTopModules/" + PortalPathUrl; ;
                //var StrTitlePortalPath = PortalPathUrl;
                var SetWhereClause = WhereClause;
                var CustomeRadListBoxObj = this.ClientID;
                var RadWindowObj = Radwindow.ClientID;
                var SetMultiSelect = (MultiSelect ? 1 : 0);
                StrParameter = (StrParameter.Replace("{", "{{")).Replace("}", "}}");
                StrParameter = (StrParameter != "" ? StrParameter.Replace(@"""", "&quot;") : "");
                str = string.Format("ShowRadWindow('{0}','{1}','{2}','{3}','{4}',{5},'{6}');", StrTitlePortalPath, CustomeRadListBoxObj, SetWhereClause, RadWindowObj, StrParameter, SetMultiSelect, FunctionJsDeveloperNameinOnClientClose);
            }
            return str;
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;
            const string jsResource = "ITC.Library.Scripts.MultiSelectControl.MultiSelectControl.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }
            scriptLoaded = true;
        }
        protected override void OnInit(EventArgs e)
        {
            RadWindowManager = new RadWindowManager();
            RadWindowManager.ID = this.ClientID + "RadWindowManager1";
            RadWindowManager.ShowContentDuringLoad = false;
            RadWindowManager.ReloadOnShow = true;
            RadWindowManager.EnableShadow = true;
            RadWindowManager.Skin = "WebBlue";
            RadWindowManager.Width = (RadWindowWidth == 0 ? 850 : RadWindowWidth);
            RadWindowManager.Height = (RadWindowHeight == 0 ? 520 : RadWindowHeight);
            RadWindowManager.OnClientClose = "OnClientClose";
            RadWindowManager.Behaviors = WindowBehaviors.Close | WindowBehaviors.Maximize;
            RadWindowManager.VisibleStatusbar = false;
            RadWindowManager.Modal = true;
            this.Controls.Add(RadWindowManager);
            Radwindow = new RadWindow();
            Radwindow.ID = this.ClientID + "_Radwindow";
            this.RadWindowManager.Windows.Add(Radwindow);


            this.ID = this.ClientID;
            this.Height = 400;
            this.Visible = (this.Width == 0 ? false : true);
            this.Width = 200;
            this.Font.Name = "Arial";
            this.Font.Size = 10;
            this.ForeColor = System.Drawing.Color.FromName("#000099");
            this.CheckBoxes = true;
            base.OnInit(e);
        }
        public void ParameterPost(object obj)
        {
            StrParameter = ITC.Library.Classes.JsonExecutor.SerializeCustomData(obj);
        }
    }
}
