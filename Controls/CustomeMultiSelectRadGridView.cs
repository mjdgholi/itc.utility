﻿using System;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.CustomeMultiSelectRadGridView.CustomeMultiSelectRadGridView.js", "text/javascript", PerformSubstitution = true)]
namespace ITC.Library.Controls
{
  public class CustomeMultiSelectRadGridView : RadGrid
    {
        private bool scriptLoaded = false;
        private HiddenField txthiddenvalue = new HiddenField();
       
        public string CustomeSelectedValue { get; set; }     
        public string CustomeMultiSelectedValue
        {
            get { return txthiddenvalue.Value; }
            set { txthiddenvalue.Value = value; }
        }


        
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            this.ToolTip = "جدول";
            base.Render(writer);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;
            const string jsResource = "ITC.Library.Scripts.CustomeMultiSelectRadGridView.CustomeMultiSelectRadGridView.js";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }
            scriptLoaded = true;
        }

        protected override void OnInit(EventArgs e)
        {
            this.ClientSettings.Selecting.AllowRowSelect = true;
            this.AllowMultiRowSelection = true;
            this.EnableViewState = true;
            GridClientSelectColumn selectColumn;
            selectColumn = new GridClientSelectColumn();

            selectColumn.UniqueName = "ClientSelectColumn";
            this.MasterTableView.Columns.Add(selectColumn);
            base.OnInit(e);
        }


        protected override void OnLoad(EventArgs e)
        {

            txthiddenvalue.ID = this.ClientID + "_txthiddenvalue";
             txthiddenvalue.Value = CustomeSelectedValue;
            this.Controls.Add(txthiddenvalue);
            Page.ClientScript.RegisterArrayDeclaration("MyHiddenField", "'" + txthiddenvalue.ClientID + "'");
            this.ClientSettings.ClientEvents.OnMasterTableViewCreated = "MasterTableViewCreated";
            this.ClientSettings.ClientEvents.OnRowSelected = "RowSelected";
            this.ClientSettings.ClientEvents.OnRowDeselected = "RowDeselected";


        }

    }
}
