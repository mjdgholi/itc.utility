﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.RadComboBox.RadComboBox.js", "text/javascript", PerformSubstitution = true)]
namespace ITC.Library.Controls
{
    [
        ToolboxData(@"<{0}:CustomRadComboBox runat=""server""> </{0}:CustomRadComboBox>")
    ]
    public class CustomRadComboBox : RadComboBox
    {
        private bool scriptLoaded = false;
        private HiddenField hiddenfieldSelectvalue = new HiddenField();
        public string KeyId
        {
            get { return hiddenfieldSelectvalue.Value; }
            set { hiddenfieldSelectvalue.Value = value; }
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(this.ToolTip))
                this.ToolTip = "جدول";
            hiddenfieldSelectvalue.RenderControl(writer);
            base.Render(writer);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ClientScriptManager scriptManager = this.Page.ClientScript;
            if (scriptLoaded) return;
            const string jsResource = "ITC.Library.Scripts.RadComboBox.RadComboBox.js";
            this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            this.Attributes.Add("onkeyUp", "return RadComboBoxConvert1256(this)");
            scriptLoaded = true;
        }

        protected override void OnInit(EventArgs e)
        {
            if (this.CheckBoxes == true)
            {
                this.OnClientLoad = "LoadComboBox";
                this.OnClientItemChecked = "OnClientItemCheckedRadComboBoxHandler";
                hiddenfieldSelectvalue = new HiddenField();
                hiddenfieldSelectvalue.ID = this.ClientID + "_hiddenfieldSelectKeyId";
                this.Controls.Add(hiddenfieldSelectvalue);
            }

        }



    }
}
