﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace ITC.Library.Controls
{
    public class AjaxButton : Button
    {
        protected override void OnLoad(EventArgs e)
        {

            if (!Page.IsPostBack)
                this.Attributes.Add("onclick", string.Format("realPostBack('{0}', ''); return false;", this.UniqueID));

            var scr = string.Format(@"
                                <script type=""text/javascript"">
                         function realPostBack(eventTarget, eventArgument)
                            {{
                                   WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(""{0}"", """", true, """", """", false, false));
                                   if(WebForm_OnSubmit())
                                    __doPostBack(eventTarget, eventArgument);

                            }}
                                </script >
                            "
                , this.ClientID);



            if (!Page.ClientScript.IsClientScriptBlockRegistered("AjaxButtonScript"))
                Page.ClientScript.RegisterStartupScript(this.GetType(), "exampleScript", scr);


            base.OnLoad(e);
        }
    }
}