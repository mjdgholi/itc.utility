﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: TagPrefix("ITC.Controls", "asp")]
[assembly: System.Web.UI.WebResource("ITC.Library.Scripts.JalaliCalendar.DatetimePicker.js", "text/javascript", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("ITC.Library.Images.JalaliCalendar.Calendar.png", "image/png")]
[assembly: System.Web.UI.WebResource("ITC.Library.Styles.JalaliCalendar.DateTimePicker.css", "text/css", PerformSubstitution = true)]


namespace ITC.Library.Controls
{
    [
        ToolboxData(@"<{0}:JalaliCalendar runat=""server""> </{0}:JalaliCalendar>")
    ]
    public class JalaliCalendar : TextBox
    {
        private bool scriptLoaded = false;

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(this.ToolTip))
                this.ToolTip = "تقویم";

            base.Render(writer);

            var imagesource = string.Format(@"<img ID='{0}_CalandarImageButton'  src='{1}' title='{2}' style='vertical-align: top;' Onclick=""displayDatePicker('{0}');"" >", this.ClientID, Page.ClientScript.GetWebResourceUrl(this.GetType(), "ITC.Library.Images.JalaliCalendar.Calendar.png"), this.ToolTip);

            writer.Write(imagesource);

        }

        protected override void OnLoad(EventArgs e)
        {
            //OnPreRender(EventArgs.Empty);

            //base.OnLoad(e);
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (scriptLoaded) return;

            //To add the JS file to the custom control
            const string jsResource = "ITC.Library.Scripts.JalaliCalendar.DatetimePicker.js";
            const string styleResource = "ITC.Library.Styles.JalaliCalendar.DateTimePicker.css";



            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(jsResource))
            {
                this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), jsResource);
            }


            var csslink = "<link rel='stylesheet' type='text/css' href='" + Page.ClientScript.GetWebResourceUrl(this.GetType(), styleResource) + "' />";
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered(csslink))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "StyleSheet", csslink);
            }
            //this.Page.Header.Controls.Add(new LiteralControl(csslink));

            scriptLoaded = true;

        }

    }
}

