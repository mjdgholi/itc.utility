﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ITC.Library.Classes;

namespace ITC.Library.Classes
{
    public class ReportParam
    {
        public string ReportPath { get; set; }
        public string Folderpath { get; set; }
        public string FolderUrl { get; set; } 
        public DataSet Dataset { get; set; }
        public List<CrystalParamater> ParamList { get; set; }
    }
}
