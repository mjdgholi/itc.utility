﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ITC.Library.Classes.ItcException
{
  public  class ItcApplicationErrorManagerException:Exception
    {
        public ItcApplicationErrorManagerException(string message): base(message)
        {
        }
    }
}
