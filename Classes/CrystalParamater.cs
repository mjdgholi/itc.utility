﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace ITC.Library.Classes
{
    public class CrystalParamater
    {

        public CrystalParamater(string reportName)
        {
            this.ReportName = reportName;
            Parameters = new List<KeyValuePair<string, string>>();
        }
        public CrystalParamater(string reportName,DataTable dataTable)
        {
            this.ReportName = reportName;
            this.DataTable = dataTable;
            Parameters = new List<KeyValuePair<string, string>>();
        }

        public string ReportName { get; private set; }
        public List<KeyValuePair<string, string>> Parameters;
        public DataTable DataTable;
    }
}
