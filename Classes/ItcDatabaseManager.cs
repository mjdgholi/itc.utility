﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Timers;
using System.Web.Configuration;


namespace ITC.Library.Classes
{
  public  class ItcDatabaseManager
    {
        private SqlConnection sqlConnection;
        private int openConnectionCount = 0;
        private static Dictionary<string, ItcDatabaseManager> dbManagers;
        private DateTime lastAccess;
        private int delay = 10;
        private System.Timers.Timer timer;

     private ItcDatabaseManager(string connectionString)
        {
            this.lastAccess = DateTime.Now;
            this.sqlConnection = new SqlConnection(connectionString);
            // InitializeComponent();
            this.timer = new System.Timers.Timer(100000);
            this.timer.Enabled = true;
            this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            this.sqlConnection.StateChange += new StateChangeEventHandler(sqlConnection_StateChange);
        }

     private ItcDatabaseManager(SqlConnection sqlConnection)
        {
            this.lastAccess = DateTime.Now;
            this.sqlConnection = sqlConnection;
            // InitializeComponent();
            this.timer = new System.Timers.Timer(100000);
            this.timer.Enabled = true;
            this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            this.sqlConnection.StateChange += new StateChangeEventHandler(sqlConnection_StateChange);
        }

     public static ItcDatabaseManager GetDbManager(string connectionString)
        {
            try
            {
                if(dbManagers==null)
                    dbManagers=new Dictionary<string, ItcDatabaseManager>();
                if (dbManagers.ContainsKey(connectionString))
                    return dbManagers[connectionString];
                ItcDatabaseManager dbManager = new ItcDatabaseManager(connectionString);          
                dbManagers[connectionString] = dbManager;
                return dbManager;    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     public static ItcDatabaseManager GetDbManager(SqlConnection connection)
        {
            try
            {
                if (dbManagers == null)
                    dbManagers = new Dictionary<string, ItcDatabaseManager>();
                if (dbManagers.ContainsKey(connection.ConnectionString))
                    return dbManagers[connection.ConnectionString];
                ItcDatabaseManager dbManager = new ItcDatabaseManager(connection);
                dbManagers[connection.ConnectionString] = dbManager;
                return dbManager;    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     public static ItcDatabaseManager GetDbManager()
        {

            try
            {
                var connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SqlConStrSelf"].ConnectionString;
                if (dbManagers == null)
                    dbManagers = new Dictionary<string, ItcDatabaseManager>();
                if (dbManagers.ContainsKey(connectionString))
                    return dbManagers[connectionString];
                ItcDatabaseManager dbManager = new ItcDatabaseManager(connectionString);
                dbManagers[connectionString] = dbManager;
                return dbManager;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            }
         
      void sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            switch (e.OriginalState)
            {
                case ConnectionState.Closed:
                    break;
                case ConnectionState.Open:
                    switch (e.CurrentState)
                    {
                        case ConnectionState.Closed:
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            this.lastAccess = DateTime.Now;
        }
      void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.openConnectionCount == 0)
            {
                if (this.sqlConnection.State == ConnectionState.Open)
                {
                    if (DateTime.Now >= this.lastAccess.AddSeconds(delay))
                        this.sqlConnection.Close();
                }
            }
        }

        private void OpenConnection()
        {
            if (this.sqlConnection.State == ConnectionState.Closed)
            {
                this.sqlConnection.Open();
            }
            this.openConnectionCount++;
        }

        private void CloseConnection()
        {
            this.openConnectionCount--;
            this.lastAccess = DateTime.Now;
        }

        public SqlCommand CreateCommand()
        {
            return this.sqlConnection.CreateCommand();
        }
        public SqlCommand CreateCommand(string commandText)
        {
            return new SqlCommand(commandText, this.sqlConnection);
        }

        public SqlDataReader ExecuteReader(SqlCommand command)
        {
            command.Connection = this.sqlConnection;
            this.OpenConnection();
            try
            {
                while (this.sqlConnection.State != ConnectionState.Open)
                    Thread.Sleep(50);
                lock (this)
                {
                    return command.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
        }
        public object ExecuteScaler(string commandText)
        {
            SqlCommand command = this.CreateCommand();
            command.CommandText = commandText;
            return this.ExecuteScaler(command);
        }
        public object ExecuteScaler(SqlCommand command)
        {
            object value = null;
            command.Connection = this.sqlConnection;
            try
            {
                lock (this)
                {
                    this.OpenConnection();
                    value = command.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
            if (value == DBNull.Value)
                return null;
            return value;
        }
        public int ExecuteNonQuery(string commandText)
        {
            SqlCommand command = this.CreateCommand();
            command.CommandText = commandText;
            return this.ExecuteNonQuery(commandText);
            //SqlCommand command = new SqlCommand(queryString, connection);
            //command.Connection.Open();
            //command.ExecuteNonQuery();
        }
        public int ExecuteNonQuery(SqlCommand command)
        {
            command.Connection = this.sqlConnection;
            try
            {
                lock (this)
                {
                    this.OpenConnection();
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
        }
        public DataTable GetTable(SqlCommand command)
        {
            lock (this)
            {
                command.Connection = this.sqlConnection;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                try
                {
                    this.OpenConnection();
                    adapter.Fill(table);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    this.CloseConnection();
                }
                return table;
            }
        }
        public DataTable GetTable(string vSqlString)
        {
            SqlCommand command = this.CreateCommand();
            command.CommandText = vSqlString;
            return this.GetTable(command);
        }

    }
}
