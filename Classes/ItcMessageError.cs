﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace ITC.Library.Classes
{
    public  class ItcMessageError
    {
        private static string[] arr;
        private static Assembly assembly;
        private static string ErrorEventPath="";



        //public static string GetMessage(Enum ErrorMessageCode)
        //{
        //    StackTrace stackTrace = new StackTrace(); // get call stack
        //    StackFrame[] stackFrames = stackTrace.GetFrames(); // get method calls (frames)

        //    assembly = stackFrames[1].GetMethod().DeclaringType.Assembly;
        //    return SetMessageText(ErrorMessageCode.ToString());
        //}

        public static string GetMessage(int ErrorMessageCode)
        {
            StackTrace stackTrace = new StackTrace(); // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames(); // get method calls (frames)

            assembly = stackFrames[1].GetMethod().DeclaringType.Assembly;
            return SetMessageText(ErrorMessageCode.ToString());
        }

        //public static string GetMessage(string MessageError)
        //{

        //    StackTrace stackTrace = new StackTrace(); // get call stack
        //    StackFrame[] stackFrames = stackTrace.GetFrames(); // get method calls (frames)
        //    assembly = stackFrames[1].GetMethod().DeclaringType.Assembly;
        //    return MessageError;
        //}


        public static string GetMessage(Exception ex)
        {
            ErrorEventPath = Regex.Split(ex.StackTrace, " at ")[1];             
            StackTrace stackTrace = new StackTrace(); // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames(); // get method calls (frames)
            assembly = stackFrames[1].GetMethod().DeclaringType.Assembly;
            if (ex.GetType().Name == "ItcApplicationErrorManagerException") // Manager ApplicationError Error By Programmer
                return ex.Message;
            else 
            return SetMessageText(ex.Message);
        }

        private static string GetProjectName(string assemblyName)
        {
            string[] array = assemblyName.Split('.');
            string projectName = "";
            if (array.Length == 1)
                projectName = assemblyName;
            else
                for (var i = 2; i <= array.Length - 1; i++)
                {
                    projectName = projectName + (i == 2 ? "" : @"\") + (array[i]);
                }
            return projectName;
        }



        private static string SetMessageText(string MessageError)
        {
            var path = "";
            var dateTimeNow = DateTime.Now.ToString();
            var userName = HttpContext.Current.User.Identity.Name;
            var pageUrl = System.Web.HttpContext.Current.Server.MapPath("");
            var errorCode = "-1";
            var errorText = "";            
            try
            {              
                arr = MessageError.Split(',');        
                var projectName = Path.GetFileNameWithoutExtension(assembly.CodeBase);
                path = System.Web.HttpContext.Current.Server.MapPath(@"~");
                path = Path.Combine(path, @"DeskTopModules\" + GetProjectName(projectName) + @"\error.txt");
                var resourcename = projectName + ".ErrorMessages.xml";
                var xmlreader = new StreamReader(assembly.GetManifestResourceStream(resourcename));
                var messageText = "";
                int messageId;
                if (Int32.TryParse(arr[0], out messageId)) // Sql And Application Error  Manager By Programmer
                {
                    messageText = (from c in XElement.Load(xmlreader).Elements("ErrorMessage")
                                   where Convert.ToInt32(c.Attribute("MessageId").Value) == messageId
                                   select c.Attribute("MessageText").Value).FirstOrDefault();
                    if (arr.Length > 1)
                        messageText=String.Format(messageText, arr[1]);

                    if (messageId.ToString() == "-1000")//No Manager Sql Error By Programmer
                    {
                        errorCode = arr[0];
                        errorText = arr[1];

                        var errorString = string.Format("ErrorType: SqlError | Date: {0} | UserName: {1} | PageUrl: {2} | ErrorCode: {3} | ErrorText: {4}  \r\n", dateTimeNow, userName, pageUrl, errorCode, errorText);
                        File.AppendAllText(path, errorString);
                    }
                }
                else //  No Manager ApplicationError Error By Programmer
                {                    
                    throw new Exception(MessageError);
                }
                return messageText;
            }
            catch (Exception exception)// No Manager ApplicationError Error By Programmer
            {                
                errorCode = "-1";
                errorText = exception.Message;
                var errorString = string.Format("ErrorType: ApplicationError | Date: {0} | UserName: {1} | PageUrl: {2} | ErrorCode: {3} | ErrorText: {4} | ErrorEventPath: {5} \r\n", dateTimeNow, userName, pageUrl, errorCode, errorText,ErrorEventPath);
                File.AppendAllText(path, errorString);
                return "خطا در سامانه ، با مدیر سیستم تماس بگیرید";
            }
        }
    }
}