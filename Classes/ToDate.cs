﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcConvert.Date;
using ITC.Library.Classes.ItcException;


namespace ITC.Library.Classes
{
    public class ItcToDate
    {
        public static string MiladiToShamsi(DateTime miladiDate)
        {
            try
            {
                return PersianDateConverter.ToPersianDate(miladiDate).ToString("yyyy/MM/dd");
            }
            catch (Exception exception)
            {
                var errortext =  exception.Message; // ValidationError
                throw new ItcApplicationErrorManagerException(errortext);
            }
        }

        public static string ShamsiToMiladi(string shamsiDate)
        {
            try
            {
                if (shamsiDate == "//" | shamsiDate == "" | shamsiDate == null)
                    return System.DBNull.Value.ToString() ;

                return PersianDateConverter.ToGregorianDateTime(shamsiDate).ToString("yyyy/MM/dd");
            }
            catch (Exception exception)
            {
                var errortext = exception.Message; // ValidationError
                throw (new ItcApplicationErrorManagerException(errortext));                
            }
        }

    }
}