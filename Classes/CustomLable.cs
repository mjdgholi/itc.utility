﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


[assembly: System.Web.UI.WebResource("ITC.Library.Styles.LableStyle.css", "text/css", PerformSubstitution = true)]

namespace Intranet.DesktopModules.Personnel
{
    public class CustomLable : Label
    {
        //protected override void OnLoad(EventArgs e)
        //{
        //    OnPreRender(EventArgs.Empty);

        //    base.OnLoad(e);
        //}
        protected override void OnPreRender(EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CssClass) )
                this.CssClass = "InputTitleLable";

            base.OnPreRender(e);

            const string styleResource = "ITC.Library.Styles.LableStyle.css";
            var csslink = "<link rel='stylesheet' type='text/css' href='" + Page.ClientScript.GetWebResourceUrl(this.GetType(), styleResource) + "' />";
            this.Page.Header.Controls.Add(new LiteralControl(csslink));

        }
    }
}
