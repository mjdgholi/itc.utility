﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Telerik.Web.UI;

namespace ITC.Library.Classes
{
    public class GridParamEntity
    {
        /// <summary>
        /// پارامترهای ست کردن گرید
        /// </summary>
        [DisplayName("گرید")]
        public RadGrid RadGrid { get; set; }

        [DisplayName("اندازه صفحه")]
        public int PageSize { get; set; }

        [DisplayName("صفحه جاری")]
        public int CurrentPage { get; set; }

        [DisplayName("شرط در اس کیو ال")]
        public string WhereClause { get; set; }

        [DisplayName("فیلد مرتب سازی رکوردها")]
        public string OrderBy { get; set; }

        [DisplayName("نوع مرتب سازی  -  سعودی یا نزولی")]
        public string SortType { get; set; }

        [DisplayName("عنوان جدول")]
        public string TableName { get; set; }

        [DisplayName("عنوان فیلد کلید")]
        public string PrimaryKey { get; set; }

        [DisplayName("شناسه رکورد انتخابی در گرید")]
        public int RowSelectId { get; set; }

        [DisplayName("شناسه رکورد انتخابی در گرید در حالتی که فیلد کلید جدول از نوع یونیک آدینتی فایر")]
        public Guid RowSelectGuidId { get; set; }

        [DisplayName("شناسه کاربر جاری")]
        public int UserId { get; set; }

        [DisplayName("جداولی که باید روی آنها دسترسی فرد چک شود")]
        public string ObjectTitlesForRowAcess { get; set; }
    }
}    