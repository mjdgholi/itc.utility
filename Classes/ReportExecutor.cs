﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using Intranet.Configuration.Settings;
using Microsoft.Reporting.WinForms;
using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Web;
using Stimulsoft.Report.Print.Design;
using Stimulsoft.Report.Web.Design;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;



namespace ITC.Library.Classes
{

    public class ReportExecutor
    {

        public enum ExportType
        {
            Pdf,
            Tiff
        }

        public static void StimulRun(string reportPath, DataSet datasource, string FolderPath, string FolderUrl, string exporttype)//4
        {
            var report = new StiReport();
            report.Load(reportPath);
            report.RegData(datasource);
            report.Render(false);
            var guid = Guid.NewGuid();
            report.ReportName = "TenderReport_" + guid.ToString().Substring(0, 8);
            report.CacheAllData = false;
            report.ClearAllStates();
            report.ReportCacheMode = StiReportCacheMode.Off;
            var service = new StiPdfExportService();
            HttpContext.Current.Response.Clear();
            var fileName = Guid.NewGuid().ToString() + ".pdf";
            var exportPath = Path.Combine(Path.GetDirectoryName(reportPath), fileName);
            service.ExportPdf(report, exportPath);
            var FileUrl = PortalSettings.PortalPath + "/" + FolderUrl + "/" + fileName;
            DeleteTempFiles(Path.GetDirectoryName(reportPath), "pdf");
            HttpContext.Current.Response.Redirect(FileUrl, false);
        }


        public static void Run(string reportPath, DataTable datasource, string FolderPath, string FolderUrl, string exporttype)//4
        {
            Run(reportPath, datasource, null, FolderPath, FolderUrl, exporttype);
        }

        //public static void Run(string reportPath, DataTable datasource, List<ReportParameter> reportParams, string FolderPath, string FolderUrl,string exporttype)//5
        //{
        //    Run(reportPath, datasource, reportParams, FolderPath, FolderUrl, exporttype);
        //}

        public static void Run(string reportPath, DataTable datasource, List<CrystalParamater> reportParams, string FolderPath, string FolderUrl, string exporttype)//6
        {
            using (var rpt = new CrystalDecisions.CrystalReports.Engine.ReportDocument())
            {
                var localReport = new LocalReport() { ReportPath = reportPath };
                byte[] dataContent;
                //try
                //{
                //string datasetName = GetDataSetName(reportPath);
                //if (string.IsNullOrEmpty(datasetName)) return;


                rpt.Load(reportPath);
                rpt.SetDataSource(datasource);
                if (reportParams != null)
                {
                    foreach (var param in reportParams)
                    {
                        if (!string.IsNullOrEmpty(param.ReportName))
                        {
                            var subReport = rpt.OpenSubreport(param.ReportName);
                            subReport.SetDataSource(param.DataTable);
                        }
                        foreach (var value in param.Parameters)
                        {
                            if (string.IsNullOrEmpty(param.ReportName))
                                rpt.SetParameterValue(value.Key, value.Value);
                            else
                                rpt.SetParameterValue(value.Key, value.Value, param.ReportName);
                        }

                    }
                }
                if (exporttype == "pdf")
                {
                    var stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    dataContent = new byte[stream.Length];
                    stream.Read(dataContent, 0, (int)stream.Length);
                    HttpContext.Current.Response.Clear();
                    var fileName = Guid.NewGuid().ToString() + ".pdf";
                    File.WriteAllBytes(Path.Combine(FolderPath, fileName), dataContent);
                    var FileUrl = PortalSettings.PortalPath + "/" + FolderUrl + "/" + fileName;
                    DeleteTempFiles(FolderPath, "pdf");
                    HttpContext.Current.Response.Redirect(FileUrl);
                }
                else if (exporttype == "xls")
                {
                    var stream = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    dataContent = new byte[stream.Length];
                    stream.Read(dataContent, 0, (int)stream.Length);
                    HttpContext.Current.Response.Clear();
                    var fileName = Guid.NewGuid().ToString() + ".xls";
                    File.WriteAllBytes(Path.Combine(FolderPath, fileName), dataContent);
                    var FileUrl = PortalSettings.PortalPath + "/" + FolderUrl + "/" + fileName;
                    DeleteTempFiles(FolderPath, "xls");
                    HttpContext.Current.Response.Redirect(FileUrl);
                }
                rpt.Close();
                rpt.Dispose();
            }
            //}
            //catch (Exception eRunReport)
            //{
            //    rpt.Close();
            //    rpt.Dispose();
            //    HttpContext.Current.Response.Clear();
            //    HttpContext.Current.Response.Write("<h1>Error running report:</h1>");
            //    HttpContext.Current.Response.Write(eRunReport.ToString());
            //    return;
            //}
            // HttpContext.Current.Response.ContentType = GetMIMEType(ExportType.Pdf);
            // HttpContext.Current.Response.AddHeader("content-disposition", string.Concat("inline; filename=", Guid.NewGuid().ToString(), GetFileName(ExportType.Pdf)));
            //var localFolderName = Path.Combine(HttpContext.Current.Server.MapPath(""), FolderPath.Replace("/", "\\"));

        }
        public static void DeleteTempFiles(string folderName, string fileExtention)
        {
            var files = from a in Directory.GetFiles(folderName, "*." + fileExtention)
                        where File.GetCreationTime(a) < DateTime.Now.AddMinutes(-20)
                        select a;
            foreach (var fileName in files)
            {
                File.Delete(fileName);
            }
        }
        #region [Private]
        private static string GetFileName(ExportType exporttype)
        {
            switch (exporttype)
            {
                case ExportType.Pdf:
                    return ".pdf";
                case ExportType.Tiff:
                    return ".Tiff";
            }
            return ".pdf";
        }

        private static string GetMIMEType(ExportType exportType)
        {
            switch (exportType)
            {
                case ExportType.Pdf:
                    return "application/pdf";
                case ExportType.Tiff:
                    return "image/tiff";

            }
            return "application/pdf";
        }


        static string GetDataSetName(string xmlPath)
        {
            var xml = new XmlDocument();
            xml.Load(xmlPath);
            var datasets = GetDatasetNodes(xml.ChildNodes);
            if (datasets != null)
                return datasets[0].Attributes["Name"].Value;
            return "";
        }


        static XmlNodeList GetDatasetNodes(System.Xml.XmlNodeList xml)
        {
            XmlNodeList result = null;

            foreach (XmlNode x in xml)
            {
                if (x.Name != "DataSets")
                {
                    result = GetDatasetNodes(x.ChildNodes);
                    if (result != null) return result;
                }
                else
                    return x.ChildNodes;
            }
            return null;
        }
        #endregion

    
    }
}
