﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Data.SqlClient;
using System.Timers;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Intranet.Common;
using Telerik.Web.UI;

namespace ITC.Library.Classes
{
    public class DatabaseManager : Component
    {
        private SqlConnection sqlConnection;
        private System.Timers.Timer timer;
        private DateTime lastAccess;
        private int delay = 10;

        private static Dictionary<string, DatabaseManager> dbManagers;


        public static DataSet ItcGetPageDataSet(GridParamEntity gridParamEntity)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 300),
                                              new SqlParameter("@SortType", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@RowSelectId", SqlDbType.Int),

                                          };
            parameters[0].Value = gridParamEntity.PageSize;
            parameters[1].Value = gridParamEntity.CurrentPage;
            parameters[2].Value = (gridParamEntity.WhereClause == "" ? DBNull.Value : (object)gridParamEntity.WhereClause);
            parameters[3].Value = (gridParamEntity.OrderBy == "" ? DBNull.Value : (object)gridParamEntity.OrderBy);
            parameters[4].Value = gridParamEntity.TableName;
            parameters[5].Value = gridParamEntity.PrimaryKey;
            parameters[6].Value = (gridParamEntity.SortType == "" ? DBNull.Value : (object)gridParamEntity.SortType);
            parameters[7].Value = (gridParamEntity.RowSelectId == -1 ? DBNull.Value : (object)gridParamEntity.RowSelectId);

            DataSet ds = intranetDB.RunProcedureDS("[pub].[p_ItcTablesGetPage]", parameters);
            //ds.Tables[0].WriteXmlSchema(@"c:\test\a.xml");
            return ds;
        }

        public static void ItcGetPage(GridParamEntity gridParamEntity)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 300),
                                              new SqlParameter("@SortType", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@RowSelectId", SqlDbType.Int),

                                          };
            parameters[0].Value = gridParamEntity.PageSize;
            parameters[1].Value = gridParamEntity.CurrentPage;
            parameters[2].Value = (gridParamEntity.WhereClause == "" ? DBNull.Value : (object) gridParamEntity.WhereClause);
            parameters[3].Value = (gridParamEntity.OrderBy == "" ? DBNull.Value : (object) gridParamEntity.OrderBy);
            parameters[4].Value = gridParamEntity.TableName;
            parameters[5].Value = gridParamEntity.PrimaryKey;
            parameters[6].Value = (gridParamEntity.SortType == "" ? DBNull.Value : (object) gridParamEntity.SortType);
            parameters[7].Value = (gridParamEntity.RowSelectId == -1 ? DBNull.Value : (object) gridParamEntity.RowSelectId);

            DataSet ds = intranetDB.RunProcedureDS("[pub].[p_ItcTablesGetPage]", parameters);
            int count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            int currentPageIndex = int.Parse(ds.Tables[1].Rows[0][1].ToString());
            int rowindex = int.Parse(ds.Tables[1].Rows[0][2].ToString());
            SetGridView(gridParamEntity.RadGrid, count, currentPageIndex, rowindex, ds);
        }

        private static void SetGridView(RadGrid radGrid, int Count, int currentPageIndex, int rowIndex, DataSet data)
        {
            radGrid.MasterTableView.CurrentPageIndex = currentPageIndex;
            radGrid.DataSource = data;
            radGrid.VirtualItemCount = Count;
            radGrid.DataBind();
            if (rowIndex != -1)
                radGrid.Items[rowIndex].Selected = true;
        }

        public static void ItcGetPageForTableGuidKey(GridParamEntity gridParamEntity)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 300),
                                              new SqlParameter("@SortType", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@RowSelectId", SqlDbType.UniqueIdentifier),

                                          };
            parameters[0].Value = gridParamEntity.PageSize;
            parameters[1].Value = gridParamEntity.CurrentPage;
            parameters[2].Value = (gridParamEntity.WhereClause == "" ? DBNull.Value : (object)gridParamEntity.WhereClause);
            parameters[3].Value = (gridParamEntity.OrderBy == "" ? DBNull.Value : (object)gridParamEntity.OrderBy);
            parameters[4].Value = gridParamEntity.TableName;
            parameters[5].Value = gridParamEntity.PrimaryKey;
            parameters[6].Value = (gridParamEntity.SortType == "" ? DBNull.Value : (object)gridParamEntity.SortType);
            parameters[7].Value = (gridParamEntity.RowSelectGuidId == new Guid() ? DBNull.Value : (object)gridParamEntity.RowSelectGuidId);

            DataSet ds = intranetDB.RunProcedureDS("[pub].[p_ItcTablesGetPageForTableGuidKey]", parameters);
            int count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            int currentPageIndex = int.Parse(ds.Tables[1].Rows[0][1].ToString());
            int rowindex = int.Parse(ds.Tables[1].Rows[0][2].ToString());
            SetGridView(gridParamEntity.RadGrid, count, currentPageIndex, rowindex, ds);
        }

        public static void ItcGetPageWithRowAccess(GridParamEntity gridParamEntity)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = new IDataParameter[]
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int), 
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1), 
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 1000),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 1000), 
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 1000),
                new SqlParameter("@SortType", SqlDbType.NVarChar, 50), 
                new SqlParameter("@RowSelectId", SqlDbType.Int),
                new SqlParameter("@WebUserId", SqlDbType.Int),
                new SqlParameter("@ObjectTitlesForRowAcess", SqlDbType.NVarChar, -1)
            };
            parameters[0].Value = gridParamEntity.PageSize;
            parameters[1].Value = gridParamEntity.CurrentPage;
            parameters[2].Value = (gridParamEntity.WhereClause == "") ? ((object)DBNull.Value) : ((object)gridParamEntity.WhereClause);
            parameters[3].Value = (gridParamEntity.OrderBy == "") ? ((object)DBNull.Value) : ((object)gridParamEntity.OrderBy);
            parameters[4].Value = gridParamEntity.TableName;
            parameters[5].Value = gridParamEntity.PrimaryKey;
            parameters[6].Value = (gridParamEntity.SortType == "") ? ((object)DBNull.Value) : ((object)gridParamEntity.SortType);
            parameters[7].Value = (gridParamEntity.RowSelectId == -1) ? ((object)DBNull.Value) : ((object)gridParamEntity.RowSelectId);
            parameters[8].Value = gridParamEntity.UserId;
            parameters[9].Value = gridParamEntity.ObjectTitlesForRowAcess;
            DataSet data = intranetDB.RunProcedureDS("[pub].[p_TablesGetPagePerRowAccess]", parameters);
            int count = int.Parse(data.Tables[1].Rows[0][0].ToString());
            int currentPageIndex = int.Parse(data.Tables[1].Rows[0][1].ToString());
            int rowIndex = int.Parse(data.Tables[1].Rows[0][2].ToString());
            SetGridView(gridParamEntity.RadGrid, count, currentPageIndex, rowIndex, data);
        }

        public static void ItcGetPageForTableGuidKeyWithRowAccess(GridParamEntity gridParamEntity)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = new IDataParameter[]
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int), new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1), 
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 100), 
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                new SqlParameter("@SortType", SqlDbType.NVarChar, 50), 
                new SqlParameter("@RowSelectId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@WebUserId", SqlDbType.Int),
                new SqlParameter("@ObjectTitlesForRowAcess", SqlDbType.NVarChar, -1)
            };
            parameters[0].Value = gridParamEntity.PageSize;
            parameters[1].Value = gridParamEntity.CurrentPage;
            parameters[2].Value = (gridParamEntity.WhereClause == "") ? ((object) DBNull.Value) : ((object) gridParamEntity.WhereClause);
            parameters[3].Value = (gridParamEntity.OrderBy == "") ? ((object) DBNull.Value) : ((object) gridParamEntity.OrderBy);
            parameters[4].Value = gridParamEntity.TableName;
            parameters[5].Value = gridParamEntity.PrimaryKey;
            parameters[6].Value = (gridParamEntity.SortType == "") ? ((object) DBNull.Value) : ((object) gridParamEntity.SortType);
            parameters[7].Value = (gridParamEntity.RowSelectGuidId == new Guid()) ? ((object) DBNull.Value) : ((object) gridParamEntity.RowSelectGuidId);
            parameters[8].Value = gridParamEntity.UserId;
            parameters[9].Value = gridParamEntity.ObjectTitlesForRowAcess;
            DataSet data = intranetDB.RunProcedureDS("[pub].[p_ItcTablesGetPageForTableGuidKeyWithRowAccess]", parameters);
            int count = int.Parse(data.Tables[1].Rows[0][0].ToString());
            int currentPageIndex = int.Parse(data.Tables[1].Rows[0][1].ToString());
            int rowIndex = int.Parse(data.Tables[1].Rows[0][2].ToString());
            SetGridView(gridParamEntity.RadGrid, count, currentPageIndex, rowIndex, data);
        }

        public static DatabaseManager GetDbManager(string connectionString)
        {
            try
            {
                if (dbManagers == null)
                    dbManagers = new Dictionary<string, DatabaseManager>();
                if (dbManagers.ContainsKey(connectionString))
                    return dbManagers[connectionString];
                DatabaseManager dbManager = new DatabaseManager(connectionString);
                dbManagers[connectionString] = dbManager;
                return dbManager;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DatabaseManager GetDbManager(SqlConnection connection)
        {
            if (dbManagers == null)
                dbManagers = new Dictionary<string, DatabaseManager>();
            if (dbManagers.ContainsKey(connection.ConnectionString))
                return dbManagers[connection.ConnectionString];
            DatabaseManager dbManager = new DatabaseManager(connection);
            dbManagers[connection.ConnectionString] = dbManager;
            return dbManager;
        }

        private DatabaseManager(string connectionString)
        {
            this.lastAccess = DateTime.Now;
            this.sqlConnection = new SqlConnection(connectionString);
            // InitializeComponent();
            this.timer = new System.Timers.Timer(100000);
            this.timer.Enabled = true;
            this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            this.sqlConnection.StateChange += new StateChangeEventHandler(sqlConnection_StateChange);
        }

        private DatabaseManager(SqlConnection sqlConnection)
        {
            this.lastAccess = DateTime.Now;
            this.sqlConnection = sqlConnection;
            // InitializeComponent();
            this.timer = new System.Timers.Timer(100000);
            this.timer.Enabled = true;
            this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            this.sqlConnection.StateChange += new StateChangeEventHandler(sqlConnection_StateChange);
        }

        public DatabaseManager(IContainer container)
        {
            container.Add(this);
            // InitializeComponent();
        }

        private void sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            switch (e.OriginalState)
            {
                case ConnectionState.Closed:
                    break;
                case ConnectionState.Open:
                    switch (e.CurrentState)
                    {
                        case ConnectionState.Closed:
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            this.lastAccess = DateTime.Now;
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.openConnectionCount == 0)
            {
                if (this.sqlConnection.State == ConnectionState.Open)
                {
                    if (DateTime.Now >= this.lastAccess.AddSeconds(delay))
                        this.sqlConnection.Close();
                }
            }
        }



        private int openConnectionCount = 0;

        private void OpenConnection()
        {
            if (this.sqlConnection.State == ConnectionState.Closed)
            {
                this.sqlConnection.Open();
            }
            this.openConnectionCount++;
        }

        private void CloseConnection()
        {
            this.openConnectionCount--;
            this.lastAccess = DateTime.Now;
        }


        public SqlCommand CreateCommand()
        {
            return this.sqlConnection.CreateCommand();
        }

        public SqlCommand CreateCommand(string commandText)
        {
            return new SqlCommand(commandText, this.sqlConnection);
        }

        public SqlDataReader ExecuteReader(SqlCommand command)
        {
            command.Connection = this.sqlConnection;
            this.OpenConnection();
            try
            {
                while (this.sqlConnection.State != ConnectionState.Open)
                    Thread.Sleep(50);
                lock (this)
                {
                    return command.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }

        }

        public int ExecuteNonQuery(string commandText)
        {
            SqlCommand command = this.CreateCommand();
            command.CommandText = commandText;
            return this.ExecuteNonQuery(command);
        }

        public int ExecuteNonQuery(SqlCommand command)
        {
            command.Connection = this.sqlConnection;
            try
            {
                lock (this)
                {
                    this.OpenConnection();
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
        }

        public DataTable GetTable(SqlCommand command)
        {
            lock (this)
            {
                command.Connection = this.sqlConnection;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                try
                {
                    this.OpenConnection();
                    adapter.Fill(table);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    this.CloseConnection();
                }
                return table;
            }
        }

        public DataTable GetTable(string vSqlString)
        {
            SqlCommand command = this.CreateCommand();
            command.CommandText = vSqlString;
            return this.GetTable(command);
        }

        public object ExecuteScaler(string commandText)
        {
            SqlCommand command = this.CreateCommand();
            command.CommandText = commandText;
            return this.ExecuteScaler(command);
        }

        public object ExecuteScaler(SqlCommand command)
        {
            object value = null;
            command.Connection = this.sqlConnection;
            try
            {
                lock (this)
                {
                    this.OpenConnection();
                    value = command.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
            if (value == DBNull.Value)
                return null;
            return value;
        }


        public static string CreateSimpleConnectionString(string path, string databaseName, string userName, string password)
        {
            return @"Persist Security Info=False;User ID=" + userName + ";Initial Catalog=" + databaseName + ";Data Source=" + path + ";password=" + password;
        }

        public static DatabaseManager GetDbManager(string path, string dbName, string userName, string password)
        {
            string conncetionString = DatabaseManager.CreateSimpleConnectionString(path, dbName, userName, password);
            return GetDbManager(conncetionString);
        }
    }
}