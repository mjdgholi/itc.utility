﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ITC.Library.Classes
{
   public class FarsiToArabic
    {
       public static string ToArabic(string Text)
       {
           char characterKaeOld = (char)1705;//ک
           char characterKaeNew = (char)1603;//ك
           char characteraeOld = (char)1740;//ی
           char characteraeNew = (char)1610;//ي
           Text = (Text.Replace(characterKaeOld, characterKaeNew));
           Text = (Text.Replace(characteraeOld, characteraeNew));
           return Text;
       }
    }
}
