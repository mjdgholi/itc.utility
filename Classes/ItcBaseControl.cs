﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;
using Intranet.Configuration.Settings;
using Intranet.Security;

namespace ITC.Library.Classes
{
    public class ItcBaseControl : ModuleControls, ITabedControl
    {
        public ItcBaseControl()
        {
            //string Controlpath = GetType().Name;
            //string ControlName = GetType().BaseType.Name;
            //string Str = "_ascx";
            //Controlpath = Controlpath.Substring(0, Controlpath.Length - (ControlName.Length + Str.Length));
            //string[] arr = Controlpath.Split('_');
            //Controlpath = "";
            //for (int i = 1; i < arr.Length - 1; i++)
            //{
            //    Controlpath = Controlpath + (i != 1 ? @"\" : "") + arr[i].ToString();
            //}
            //ControlName = Controlpath + @"\" + ControlName;
            //CheckAccessInPage(ControlName);
        }

        public static void CheckAccessInPage(string PageName)
        {
            if (1==1)
            {
                var UserId = HttpContext.Current.User.Identity.Name;
                var SqlConnection = ITC.Library.Classes.ItcDatabaseManager.GetDbManager();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[Pub].[p_CheckAccessInPage]";
                sqlCommand.Parameters.Add("@UserId", SqlDbType.NVarChar, 50);
                sqlCommand.Parameters.Add("@PageName", SqlDbType.NVarChar, 500);
                sqlCommand.Parameters["@UserId"].Value = UserId;
                sqlCommand.Parameters["@PageName"].Value = PageName;
                var Count = int.Parse(SqlConnection.ExecuteScaler(sqlCommand).ToString());
                if (Count <= 0)
                {
                    HttpContext.Current.Response.Redirect(IntranetUI.BuildUrl("~/accessdenied.aspx"));
                }
            }
            
        }

        public static void CheckAccessWithRowAccess(string pageName)
        {
            var userId = HttpContext.Current.User.Identity.Name;
            var sqlDbManager = ITC.Library.Classes.ItcDatabaseManager.GetDbManager();
            var sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "[Pub].[p_PubItcMenuCheckHasAccessToPage]";
            sqlCommand.Parameters.Add("@UserId", SqlDbType.NVarChar, 50);
            sqlCommand.Parameters["@UserId"].Value = userId;
            var dtResult= sqlDbManager.GetTable(sqlCommand);
            var resultEnum = dtResult.AsEnumerable();
            var count=resultEnum.Where(t => t.Field<string>("ItcMenuPageName") == pageName).ToArray().Count();
            if (count <= 0)
            {
                HttpContext.Current.Response.Redirect(IntranetUI.BuildUrl("~/accessdenied.aspx"));
            }            
        }

        public virtual void InitControl()
        {
           
        }


    }
}