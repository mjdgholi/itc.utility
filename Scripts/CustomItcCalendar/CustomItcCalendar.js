﻿
// تابع فراخواني تاريخ از تقويم
function OpenDate(TDObj, e) {
    $("#" + TDObj).datepicker({
        onClose: function () {
            this.focus();
            $(this).datepicker('destroy');
        },
        onSelect: function () {
            this.focus();
        }
    });
    $("#" + TDObj).datepicker('show');
}

// تابع کنترل تاريخ
function DateValidation(ctrl) {

    if (ctrl.value == "") return;

    if (ctrl.value.length != 10) {
        alert('فرمت تاريخ 10 حرفي است ,مثال 1388/09/09');
        ctrl.value = "";

        return false;
    }
    if (ctrl.value.charAt(4) != '/' || ctrl.value.charAt(7) != '/') {
        alert('فرمت تاريخ اشتباه وارد شده است ,مثال 1388/09/09');
        ctrl.value = "";

        return false;
    }
    var mydate = ctrl.value;
    var parts = mydate.split('/');

    var year = parts[0];
    var month = parts[1];
    var day = parts[2];

    if (year > 1500 || year < 1200) {
        alert(" سال اشتباه وارد شده است , محدوده سال از 1200 کوچکتر و از 1500 بزرگتر تمي تواند باشد");
        ctrl.value = "";
        return false;
    }

    if (month > 12 || month < 1) {
        alert("ماه اشتباه وارد شده است.");
        ctrl.value = "";
        return false;
    }
    if (day > 31 || day < 1) {
        alert(" روز اشتباه وارد شده است");
        ctrl.value = "";
        return false;
    }

    if ((month >= 1) && (month <= 6)) {
        if ((day >= 1) && (day <= 31)) {
            return true;
        }
        else {
            alert(" روز اشتباه وارد شده است");
            ctrl.value = "";
        }

    }
    else if ((month >= 7) && (month <= 11)) {
        if ((day >= 1) && (day <= 30)) {
            return true;
        }
        else {
            alert(" روز اشتباه وارد شده است");
            ctrl.value = "";
        }
    }
    else if (month == 12) {
        if ((day >= 1) && (day <= 29)) {
            return true;
        }
        else if (day == 30) {
            if (isLeapYear(year))
                return true;
            else {
                alert(" سال وارد شده کبيسه نيست !");
                ctrl.value = "";
            }
        }
    }

}





function isLeapYear(strYear) {
    strYear = parseFloat(strYear);
    strYear += 1;
    if ((strYear == null) || (strYear == ''))
        return false;

    var strYearFormat = new RegExp('^([0-9]{4}|[0-9]{2})');

    if (!strYearFormat.test(strYear))
        return false;

    var intYear = parseInt(strYear, 10);

    if (intYear % 400 == 0)
        return true;
    if (intYear % 100 == 0)
        return false;
    if (intYear % 4 == 0)
        return true;

    return false;
}