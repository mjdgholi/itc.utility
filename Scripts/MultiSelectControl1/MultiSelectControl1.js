﻿var RadListBoxId;
var hiddenfieldSelectKeyId; 
var hiddenfieldTitle;
var MyGeneralListBoxValue = new Array();
var MyGeneralListBoxText = new Array();
var hiddenfieldGeneralKeyId;

function ShowRadWindow(portalPath, ListBoxIdObject, WhereClause, RadWindowObj, StrParameter) {  
    var oWnd = $find(RadWindowObj);
    oWnd.setUrl(portalPath + "?Parameter=" + StrParameter);
    oWnd.show();
    oWnd.WhereClause = WhereClause;
    RadListBoxId = ListBoxIdObject;
    hiddenfieldSelectKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldSelectKeyId';
    hiddenfieldTitle = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldTitle';
    hiddenfieldGeneralKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldGeneralKeyId'; 
       
    }


    function OnClientClose(oWnd, args) {
  
        MyGeneralListBoxText = [];
        MyGeneralListBoxValue = [];
        var arg = args.get_argument();
        if (arg) {
            var items = $find(RadListBoxId).get_items();          
            items.clear();
            var mySplitTitle = arg.Title.split(",");
            var mySplitValu = arg.KeyId.split(",");            
           for (i = 0; i < mySplitTitle.length; i++) {
               var item = new Telerik.Web.UI.RadListBoxItem();
                item.set_text(mySplitTitle[i]);
                item.set_value(mySplitValu[i]);
                item.set_checked(true);
                $find(RadListBoxId).get_items().add(item);
            }

            document.getElementById(hiddenfieldSelectKeyId).value = arg.KeyId;
            document.getElementById(hiddenfieldGeneralKeyId).value = arg.KeyId;
            document.getElementById(hiddenfieldTitle).innerText = arg.Title;    
        }
        items.forEach(function(itm) { MyGeneralListBoxText.push(itm.get_text()); MyGeneralListBoxValue.push(itm.get_value()); })      
    }





