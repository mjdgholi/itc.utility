﻿function RadComboBoxConvert1256(obj) {
    comboBox = document.getElementById(obj.id + "_Input");
    var the_length = comboBox.value.length;
    var last_char = comboBox.value.charAt(the_length - 1);
    if (last_char == "ک" | last_char == "ی") {
        comboBox.value = comboBox.value.replace("ک", "ك");
        comboBox.value = comboBox.value.replace("ی", "ي");
    }
}

function LoadComboBox(sender, args) {    
    var RadComboBoxId = sender.get_id();
    var itemsAll = sender.get_items();    
    hiddenfieldSelectKeyId = RadComboBoxId + '_' + RadComboBoxId + '_hiddenfieldSelectKeyId';    
    var myArray = document.getElementById(hiddenfieldSelectKeyId).value.split(',');
    itemsAll.forEach(function (itm) {
        if (include(myArray, itm.get_value()) == true)
            itm.set_checked(true);
    });    
}

function OnClientItemCheckedRadComboBoxHandler(sender, eventArgs) {
    var RadComboBoxId = sender.get_id();
    hiddenfieldSelectKeyId = RadComboBoxId + '_' + RadComboBoxId + '_hiddenfieldSelectKeyId';
    if (!eventArgs.get_item().get_checked()) {
        DeleteKeyIdinHiddenField(eventArgs.get_item().get_value());
    }
    else {        
        InsertKeyIdinHiddenField(eventArgs.get_item().get_value());
    }
}

function DeleteKeyIdinHiddenField(ItemKeyId) {

    var MyArray = document.getElementById(hiddenfieldSelectKeyId).value.split(",");   
    var str = '';
    var hiddenfieldvalue = '';
    for (var i = 0; i < MyArray.length; i++) {
        if (MyArray[i] == ItemKeyId)
            MyArray.splice(i, 1);
    }
    for (var i = 0; i < MyArray.length; i++) {
        if (i != 0)
            str = ',';
        hiddenfieldvalue = hiddenfieldvalue + str + MyArray[i];
    }
    document.getElementById(hiddenfieldSelectKeyId).value = hiddenfieldvalue;
}

function InsertKeyIdinHiddenField(ItemKeyId) {
    var MyArray = [];
    if (document.getElementById(hiddenfieldSelectKeyId).value != '')
        MyArray = document.getElementById(hiddenfieldSelectKeyId).value.split(",");
    var str = '';
    var hiddenfieldvalue = document.getElementById(hiddenfieldSelectKeyId).value.split(",");
    if (include(MyArray, ItemKeyId) != true) {
        if (MyArray.length > 0)
            str = ',';
        hiddenfieldvalue = hiddenfieldvalue + str + ItemKeyId;
        MyArray.push(ItemKeyId);
    }
    document.getElementById(hiddenfieldSelectKeyId).value = hiddenfieldvalue;
}



function include(arr, obj) {
    if (arr.length == 0)
        return false;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == obj)
            return true;
    }
}


