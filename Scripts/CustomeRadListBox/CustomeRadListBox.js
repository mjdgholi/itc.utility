﻿var MyGeneralListBoxValue = new Array();
var MyGeneralListBoxText = new Array();
var RadListBoxId;
var itemsALL;
var CheckBoxAllId;
var hiddenfieldSelectKeyId;
var hiddenfieldTitle;
var hiddenfieldGeneralKeyId;


function LoadListBox(sender, args) {
    RadListBoxId = sender.get_id();
    hiddenfieldSelectKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldSelectKeyId';
    document.getElementById(hiddenfieldSelectKeyId).value = '';    
    SaveTitleAndIdListBoxGeneral(RadListBoxId);
}

function SearchINListBox(txtSearchObj, ListBoxObjectId) {
    RadListBoxId = ListBoxObjectId.getAttribute('id');
    hiddenfieldGeneralKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldGeneralKeyId';
    hiddenfieldTitle = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldTitle';   
    MyGeneralListBoxText = document.getElementById(hiddenfieldTitle).value.split(",");    
    MyGeneralListBoxValue = document.getElementById(hiddenfieldGeneralKeyId).value.split(",");
    itemsALL = $find(RadListBoxId).get_items();
    itemsALL.clear();
    for (i = 0; i < MyGeneralListBoxText.length; i++) {
        var item = new Telerik.Web.UI.RadListBoxItem();
        item.set_text(MyGeneralListBoxText[i]);
        item.set_value(MyGeneralListBoxValue[i]);
        itemsALL.add(item);
    }    
    var MyListBoxNotItemSearch = new Array();
    hiddenfieldSelectKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldSelectKeyId';
    CheckBoxAllId = RadListBoxId + '_SelectAll';

    if (txtSearchObj.value != 'جستجو') {
        var txtSearch = Convert1256(txtSearchObj.value);
        itemsALL.forEach(function (itm) {
            if (itm.get_text().search(txtSearch) == -1)
                MyListBoxNotItemSearch.push(itm);
        });
        for (var i = 0; i < MyListBoxNotItemSearch.length; i++) {
            itemsALL.remove(MyListBoxNotItemSearch[i]);
        }
    }
    var MyArray = document.getElementById(hiddenfieldSelectKeyId).value.split(",");
    itemsALL.forEach(function (itm) {
        if (include(MyArray, itm.get_value()) == true)
            itm.set_checked(true);
    });
    var Count = 0;
    itemsALL.forEach(function (itm) {
        if (itm.get_checked() == true)
            Count = Count + 1;
    });

    if (Count != itemsALL.get_count())
        document.getElementById(CheckBoxAllId).checked = false;
}


function SaveTitleAndIdListBoxGeneral(RadListBoxId) {    
    hiddenfieldGeneralKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldGeneralKeyId';
    hiddenfieldTitle = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldTitle';
    document.getElementById(hiddenfieldTitle).value = '';
    document.getElementById(hiddenfieldGeneralKeyId).value = '';
    var str = '';

    $find(RadListBoxId).get_items().forEach(function (itm) {
        if (document.getElementById(hiddenfieldTitle).value != '') {
            str = ',';
        }      
        document.getElementById(hiddenfieldTitle).value = (document.getElementById(hiddenfieldTitle).value + str + itm.get_text());
        document.getElementById(hiddenfieldGeneralKeyId).value = (document.getElementById(hiddenfieldGeneralKeyId).value+str + itm.get_value());
    });   
}

function Convert1256(txtSearch) {
    txtSearch = txtSearch.replace(new RegExp("ک", 'g'), "ك");
    txtSearch = txtSearch.replace(new RegExp("ی", 'g'), "ي");
    return txtSearch;
}

function SetTitleSearch(txtSearchObj) {
    if (txtSearchObj.value == '')
        txtSearchObj.value = 'جستجو';
}

function ClearTitleSearch(txtSearchObj) {
    if (txtSearchObj.value == 'جستجو')
        txtSearchObj.value = '';
}

function SelectDeSelectAll(CheckBoxObj, ListBoxObjectId) {
    RadListBoxId = ListBoxObjectId.getAttribute('id');
    itemsALL = $find(RadListBoxId).get_items();
    hiddenfieldSelectKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldSelectKeyId';    
    var str = ',';
    var hiddenfieldvalue = '';
    itemsALL.forEach(function (itm) {
        itm.set_checked(CheckBoxObj.checked);
    });
    if (CheckBoxObj.checked) {
        itemsALL.forEach(function (itm) { {
                                              hiddenfieldvalue = hiddenfieldvalue + itm.get_value() + str; } });
    }
    hiddenfieldvalue = hiddenfieldvalue.substring(0, hiddenfieldvalue.length - 1); 
    document.getElementById(hiddenfieldSelectKeyId).value = hiddenfieldvalue;    
}

function OnClientItemCheckedHandler(sender, eventArgs) {
    RadListBoxId = sender.get_id();
    hiddenfieldSelectKeyId = RadListBoxId + '_' + RadListBoxId + '_hiddenfieldSelectKeyId';
    CheckBoxAllId = RadListBoxId + '_SelectAll';
    if (!eventArgs.get_item().get_checked()) {
        document.getElementById(CheckBoxAllId).checked = false;
        DeleteKeyIdinHiddenField(eventArgs.get_item().get_value());
    }
    else
        InsertKeyIdinHiddenField(eventArgs.get_item().get_value());
    if (sender.get_checkedItems().length == sender.get_items().get_count())
        document.getElementById(CheckBoxAllId).checked = true;
}

function DeleteKeyIdinHiddenField(ItemKeyId) {
    var MyArray = document.getElementById(hiddenfieldSelectKeyId).value.split(",");
    var str = '';
    var hiddenfieldvalue = '';
    for (var i = 0; i < MyArray.length; i++) {
        if (MyArray[i] == ItemKeyId)
            MyArray.splice(i, 1);
    }
    for (var i = 0; i < MyArray.length; i++) {
        if (i != 0)
            str = ',';
        hiddenfieldvalue = hiddenfieldvalue + str + MyArray[i];
    }
    document.getElementById(hiddenfieldSelectKeyId).value = hiddenfieldvalue;
}

function InsertKeyIdinHiddenField(ItemKeyId) {
    var MyArray = [];
    if (document.getElementById(hiddenfieldSelectKeyId).value != '')
        MyArray = document.getElementById(hiddenfieldSelectKeyId).value.split(",");
    var str = '';
    var hiddenfieldvalue = document.getElementById(hiddenfieldSelectKeyId).value.split(",");
    if (include(MyArray, ItemKeyId) != true) {
        if (MyArray.length > 0)
            str = ',';
        hiddenfieldvalue = hiddenfieldvalue + str + ItemKeyId;
        MyArray.push(ItemKeyId);
    }
    document.getElementById(hiddenfieldSelectKeyId).value = hiddenfieldvalue;
}


function include(arr, obj) {
    if (arr.length == 0)
        return false;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == obj)
            return true;
    }
}

;




